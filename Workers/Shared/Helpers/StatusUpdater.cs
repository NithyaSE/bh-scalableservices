﻿using System;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;

namespace BH.ScalableServices.Workers.Shared.Helpers
{
    public static class StatusUpdater
    {
        public static IScalableServiceWebClient ServiceWebClient { get; set; }

        public static void SetIsExecuting(string runId,string LoggingName, string message=null)
        {
            // when worker picks the message -//The is_queued Boolean is set to false.// The is_executing Boolean is set to true.
            UpdateStatus(new { is_queued = false, is_executing = true, is_dehydrated = false }, runId);
            //A log entry is written.
            WriteLogEntry(new ScalableServiceLogEntry
            {
                Timestamp = DateTime.UtcNow,
                Source = LoggingName,
                Message = string.IsNullOrEmpty(message)?"Message read from Queue and submitted for processing" :message
            }, runId);

        }

        public static void SetIsException(string runId, string LoggingName, string message=null)
        {
            UpdateStatus(new { is_executing = false, is_exception = true }, runId);
            WriteLogEntry(new ScalableServiceLogEntry
            {
                Timestamp = DateTime.UtcNow,
                Source = LoggingName,
                Message = string.IsNullOrEmpty(message)?"Exception Occured":message
            }, runId);
        }

        public static void SetSuccessfullExecution(string runId, string LoggingName, string message=null)
        {

            UpdateStatus(new { is_executing = false , is_exception = false }, runId);
            WriteLogEntry(new ScalableServiceLogEntry
            {
                Timestamp = DateTime.UtcNow,
                Source = LoggingName,
                Message = string.IsNullOrEmpty(message)?"Message Processed Successfully":message
            }, runId);
        }

        public static void SetIsWarning(string runId, string LoggingName, string message=null)
        {

            UpdateStatus(new { is_warning = true }, runId);
            WriteLogEntry(new ScalableServiceLogEntry
            {
                Timestamp = DateTime.UtcNow,
                Source = LoggingName,
                Message = string.IsNullOrEmpty(message)?"Warning Occured":message
            }, runId);
        }

        public static void SetIsDehydrated(string runId, string LoggingName, string message = null)
        {

            UpdateStatus(new { is_executing = false, is_dehydrated = true }, runId);
            WriteLogEntry(new ScalableServiceLogEntry
            {
                Timestamp = DateTime.UtcNow,
                Source = LoggingName,
                Message = string.IsNullOrEmpty(message) ? "Dehydrating" : message
            }, runId);
        }
        public static void ResetStatusOnVisible(string runId, string LoggingName, string message = null)
        {

            UpdateStatus(new { is_executing = false, is_queued=true }, runId);
        }
        public static void UpdateStatus(object status, string runId)
        {
            ServiceWebClient.UpdateStatus(status, runId);
        }

        private static void WriteLogEntry(ScalableServiceLogEntry entry, string runId)
        {
            ServiceWebClient.PostLogEntry(entry, runId);

        }

    }
}
