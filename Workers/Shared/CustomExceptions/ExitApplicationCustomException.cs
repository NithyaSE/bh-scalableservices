﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace BH.ScalableServices.Workers.Shared.CustomExceptions
{/// <summary>
/// Exception that indicates the worker needs to exit
/// </summary>

   [Serializable]
    public class ExitApplicationCustomException : Exception
    {
        public string ResourceReferenceProperty { get; set; }
        public ExitApplicationCustomException()
        {
        }

        public ExitApplicationCustomException(string message)
            : base(message)
        {
        }

        public ExitApplicationCustomException(string message, Exception inner)
            : base(message, inner)
        {
        }
        protected ExitApplicationCustomException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ResourceReferenceProperty = info.GetString("ResourceReferenceProperty");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("ResourceReferenceProperty", ResourceReferenceProperty);
            base.GetObjectData(info, context);
        }
    }
}


