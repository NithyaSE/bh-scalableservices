﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.VizService.v1_0.Models
{
   public class VisualizationServiceTaskWorkingSet
    {
        [JsonProperty("dashboard_instance")]
        public string DashboardInstance { get; set; }
        [JsonProperty("plugin_flavors")]
        public string PluginFlavors { get; set; }
        [JsonProperty("visualization_service_run_id")]
        public string VisualizationServiceRunId { get; set; }
        [JsonProperty("run_outputs")]
        public Dictionary<string,object> RunOutputs { get; set; }
        [JsonProperty("check_interval")]
        public string CheckInterval { get; set; }
    }
}
