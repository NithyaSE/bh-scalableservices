﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using BH.ScalableServices.Brokers.VisualziationService.SDK;
using BH.ScalableServices.Workers.Plugins.VisualizationService.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.VizService.v1_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using BH.Visualization.Dashboard.API.Shared.New_Models.Dashboard;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.VizService.v1_0
{
    public class VisualizationServiceTask : WorkflowTask
    {
        private IVisualziationAPIClient _visualziationApiClient;

        public VisualizationServiceTask()
        {
            var codeBase = typeof(VisualizationServiceTask).Assembly.CodeBase;
            var uri = new UriBuilder(codeBase);
            _visualziationApiClient = new VisualziationAPIClient(); // creds read from worker router config
        }

        //TODO : Do we need to dehydrate -- yes
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            try
            {
                var vizServiceTaskWorkingSet = workingSet.ToObject<VisualizationServiceTaskWorkingSet>();
                Console.WriteLine(
                    $"Running Visualization Service Task. The name is : {SystemTaskVariables.WorkflowTaskInstanceName}");
                //first run
                var checkInterval = vizServiceTaskWorkingSet.CheckInterval;
                var regex = new Regex(
                    "^P(?!$)(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?(T(?=\\d+[HMS])(\\d+H)?(\\d+M)?(\\d+S)?)?$");
                var match = regex.Match(checkInterval);
                if (!match.Success)
                {
                    throw new Exception(
                        $"Invalid format of check interval {checkInterval}. Only ISO interval format accepted");
                }
                if (!SystemTaskVariables.IsRehydrated)
                {
                    var dashboardInstance =
                        JsonConvert.DeserializeObject<DashboardInstance>(vizServiceTaskWorkingSet.DashboardInstance);
                    var pluginFlavors =
                        JsonConvert.DeserializeObject<List<string>>(vizServiceTaskWorkingSet.PluginFlavors);
                    var runDetails = _visualziationApiClient.PostStart(new VisualizationServiceRequest()
                    {
                        DashboardInstance = dashboardInstance,
                        PluginFlavors = pluginFlavors
                    });

                    var runId = runDetails.RunId;
                    vizServiceTaskWorkingSet.VisualizationServiceRunId = runId;
                    LogEntryWriter.LogMessage(
                        $"RunId of the Visualization Service Request: {runId}. Use this RunId to get the run details.",
                        SystemTaskVariables.ProcessRunId);
                    return new DehydrateRunTaskReturn
                    {
                        CheckInterval = checkInterval,
                        WorkingSet = JObject.FromObject(vizServiceTaskWorkingSet)
                    };
                }
                //rehydrated task
                LogEntryWriter.LogMessage($"Rehydrating task : {SystemTaskVariables.WorkflowTaskInstanceName}", SystemTaskVariables.ProcessRunId);
                var runDetailsVizService = _visualziationApiClient.GetDetails(runId: vizServiceTaskWorkingSet.VisualizationServiceRunId);
                if (runDetailsVizService.Status.IsException == true || runDetailsVizService.Status.IsWarning == true)
                {
                    LogEntryWriter.LogMessage($"Error in visualization service run. Check RunId : {runDetailsVizService.RunId} for details.", SystemTaskVariables.ProcessRunId);
                    var reason = runDetailsVizService.Status.IsException == true ? ("is_exception = TRUE") : runDetailsVizService.Status.IsWarning == true ? "is_warning = TRUE" : "";
                    throw new Exception($"Visualization service run returned with {reason}. Check RunId : '{runDetailsVizService.RunId}' for details.");
                }
                if (runDetailsVizService.Status.IsQueued == false && runDetailsVizService.Status.IsExecuting == false && runDetailsVizService.Status.IsDehydrated == false)
                {
                    LogEntryWriter.LogMessage($"Visualization Service run complete. Check RunId : '{runDetailsVizService.RunId}' for details.", SystemTaskVariables.ProcessRunId);
                }
                vizServiceTaskWorkingSet.RunOutputs = _visualziationApiClient.GetOutputs(vizServiceTaskWorkingSet.VisualizationServiceRunId); ;
                return new FinishRunTaskReturn
                {
                    WorkingSet = JObject.FromObject(vizServiceTaskWorkingSet)
                };
            }
            catch (Exception exp)
            {
                LogEntryWriter.LogMessage(exp.Message, SystemTaskVariables.ProcessRunId);
                throw;
            }

        }
    }
}
