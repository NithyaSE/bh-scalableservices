﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Init
{
    public class Program
    {
        private static string nugetConfigContents =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<configuration>\r\n  <config>\r\n    <add key=\"repositoryPath\" value=\"packages\" />\r\n  </config>\r\n  <packageSources>\r\n    <add key=\"nuget.org\" value=\"https://api.nuget.org/v3/index.json\" protocolVersion=\"3\" />\r\n\t<add key=\"Myget\" value=\"https://www.myget.org/F/schneider-electric/api/v3/index.json\" protocolVersion=\"3\" />\r\n  </packageSources>\r\n  <packageSourceCredentials>\r\n    <Myget>\r\n        <add key=\"Username\" value=\"EtiAggarwal\" />\r\n        <add key=\"ClearTextPassword\" value=\"mygetPASS123\" />\r\n    </Myget>\r\n</packageSourceCredentials>\r\n</configuration>"
            ;
        public static void Main(string[] args)
        {
            // Lookup the nuget.config in the current directory
          // update the packages to full directory path
            nugetConfigContents = nugetConfigContents.Replace("value=\"packages\"", $"value=\"{Path.GetFullPath(".")}\\packages\"");
            // lookup sln file and move the nuget.config there.
            var solutionDirectory = TryGetSolutionDirectoryInfo(Path.GetFullPath("."))?.FullName;
            if (solutionDirectory == null)
            {
                Console.WriteLine("No .sln file found in the parent directories.");
                Console.WriteLine("Please provide the location of the solution file..");
                solutionDirectory = Console.ReadLine();
            }
            var filePath = $"{solutionDirectory}/nuget.config";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            Console.WriteLine($"Writing nuget.config file at '{filePath}'");
            File.WriteAllText(filePath,nugetConfigContents);
        }

        public static DirectoryInfo TryGetSolutionDirectoryInfo(string currentPath = null)
        {
            var directory = new DirectoryInfo(
                currentPath ?? Directory.GetCurrentDirectory());
            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory;
        }
    }
}
