﻿using System.Collections.Generic;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.Models
{
    public class DocProductionBody
    {
        public DocProductionBody()
        {
            content_masters = new List<string>();
            plain_text_tag_mappings = new List<Tag>();
            outputs= new List<string>();
        }
    
        public string template_file_path { get; set; }

        public string plain_text_tag_mappings_csv { get; set; }

        public List<Tag> plain_text_tag_mappings { get; set; }
        public string images_path { get; set; }

        public List<string> content_masters { get; set; }

        public List<string> outputs { get; set; }
        
        public string timeout { get; set; }

        public string culture_info_code { get; set; }


    }
}
