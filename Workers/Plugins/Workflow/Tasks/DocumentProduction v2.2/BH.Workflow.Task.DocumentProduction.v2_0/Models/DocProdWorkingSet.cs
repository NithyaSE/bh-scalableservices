﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.Models
{
   public class DocProdWorkingSet
    {
        [JsonProperty("cultureinfo_code")]
        public string CultureInfoCode { get; set; }
        [JsonProperty("template_file_path")]
        public string TemplateFilePath { get; set; }

        [JsonProperty("plain_text_tag_mappings")]
        public List<Tag> PlainTextTagMappings { get; set; }
        [JsonProperty("images_path")]
        public string ImagesPath { get; set; }
        [JsonProperty("content_masters")]
        public List<string> ContentMasters { get; set; }
        [JsonProperty("outputs")]
        public List<string> GeneratedFilePaths { get; set; }
        [JsonProperty("timeout")]
        public string Timeout { get; set; }
        [JsonProperty("check_interval")]
        public string CheckInterval { get; set; }
        [JsonProperty("doc_prod_run_id")]
        public string DocProdRunId { get; set; }
    }
}
