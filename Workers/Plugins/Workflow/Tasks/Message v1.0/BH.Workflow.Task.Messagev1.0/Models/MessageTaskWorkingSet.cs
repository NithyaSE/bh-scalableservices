﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Message.v1_0.Models
{
   public class MessageTaskWorkingSet
    {
        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }
        [JsonProperty("bh_messages")]
        public string BHMessages { get; set; }
        [JsonProperty("message_relay_dll_names")]
        public string RelayDllNames { get; set; }
    }
}
