﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using BH.ScalableServices.Brokers.Workflow.SDK;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcessOneIteration.v2_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcessOneIteration.v2_0
{
    public class RunSubProcesses : WorkflowTask
    {
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            Console.WriteLine("Running Subprocess One Iteration Task! The name is: " + SystemTaskVariables.WorkflowTaskInstanceName);
            var subProcessSingleIterWorkingSet = workingSet.ToObject<SubProcSingleIterWorkingSet>();
            //first run
            var checkInterval = subProcessSingleIterWorkingSet.CheckInterval;
            var regex = new Regex("^P(?!$)(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?(T(?=\\d+[HMS])(\\d+H)?(\\d+M)?(\\d+S)?)?$");
            var match = regex.Match(checkInterval);
            if (!match.Success)
            {
                throw new Exception($"Invalid format of check interval {checkInterval}. Only ISO interval format accepted");
            }
            if (!SystemTaskVariables.IsRehydrated)
            {
                var subProcessProcessVariableMappings = subProcessSingleIterWorkingSet.SubProcessProcessVariableMappings;
                var subProcessProcessName = subProcessSingleIterWorkingSet.SubProcessProcessName;
                var subProcRegistry = new SubProcessRegistryItem();
                InitSubProcess(subProcRegistry, subProcessProcessName, subProcessProcessVariableMappings);
                subProcessSingleIterWorkingSet.SubProcessRegistry = subProcRegistry;
                return new DehydrateRunTaskReturn()
                {
                    CheckInterval = checkInterval,
                    WorkingSet = JObject.FromObject(subProcessSingleIterWorkingSet)
                };
            }
            else
            {
                //rehydrated task
                LogEntryWriter.LogMessage($"Rehydrating task : {SystemTaskVariables.WorkflowTaskInstanceName}",SystemTaskVariables.ProcessRunId);
                var subProcRegistry = subProcessSingleIterWorkingSet.SubProcessRegistry;
                CheckSubProcRunStatus(subProcRegistry);
                if (!subProcRegistry.IsFinished)
                    return new DehydrateRunTaskReturn()
                    {
                        CheckInterval = checkInterval,
                        WorkingSet = JObject.FromObject(subProcessSingleIterWorkingSet)
                    };
                var outputs= CollectSubProcessOutputs(subProcRegistry);
                Directory.Delete(subProcRegistry.Location, true);
                LogEntryWriter.LogMessage($"Subprocess complete.", SystemTaskVariables.ProcessRunId);
                foreach (var output in outputs)
                {
                    workingSet[output.Key] = JToken.FromObject(output.Value);
                }
                return new FinishRunTaskReturn()
                {
                    WorkingSet = workingSet
                };
            }
        }
        private void CheckSubProcRunStatus(SubProcessRegistryItem subProcRegistryItem)
        {
          //get details, if not done break
            var runDetails = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).GetDetails(subProcRegistryItem.RunId, false);
            if (runDetails.Status.IsException == true || runDetails.Status.IsWarning == true)
            {
                LogEntryWriter.LogMessage($"Error in spawned subprocess : {runDetails.ProcessInstanceName}. Check RunId : {runDetails.RunId} for details.", SystemTaskVariables.ProcessRunId);
                var reason = runDetails.Status.IsException == true ? ("is_exception = TRUE") : runDetails.Status.IsWarning == true ?  "is_warning = TRUE":"";
                throw new Exception($"An exception was raised in the subprocess task '{SystemTaskVariables.WorkflowTaskInstanceName}' as the spawned task completed with '{reason}'. Check RunId : '{runDetails.RunId}' for details.");
            }
            if (runDetails.Status.IsQueued != false || runDetails.Status.IsExecuting != false ||
                runDetails.Status.IsDehydrated != false) return;
            LogEntryWriter.LogMessage($"Subprocess : '{runDetails.ProcessInstanceName}' complete. Check RunId : '{runDetails.RunId}' for details.", SystemTaskVariables.ProcessRunId);
            subProcRegistryItem.IsFinished = true;
        }
        private Dictionary<string,object> CollectSubProcessOutputs(SubProcessRegistryItem subProcRegistryItem)
        {
            var outputPvValueDict = new Dictionary<string, object>(); // <outputpv,value>
           //get outputs and put into dictionary
            var outputs = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).GetProcessOutputs(subProcRegistryItem.RunId); //<outputpv,value>
            foreach (var outputvar in outputs)
            {
                    if (!outputPvValueDict.ContainsKey(outputvar.Key))
                        outputPvValueDict.Add(outputvar.Key, outputvar.Value);
            }
            return outputPvValueDict;
        }
        private void InitSubProcess(SubProcessRegistryItem subProcRegistry,string baseProcessName, List<KeyValueObject<object>> subProcessProcessVariableMappings)
        {
            //name for the new subprocess
            var name = baseProcessName;
            //ceate new folder
            var newLocation = Path.Combine(Settings.ProcessBundlesSubProcLocation, SystemTaskVariables.ProcessRunId, name);
            var partialPathToSubProcess = $"{SystemTaskVariables.ProcessRunId}";
            if (!Directory.Exists(newLocation))
                Directory.CreateDirectory(newLocation);
            //copy over base to new location
            var oldLocation = Path.Combine(Settings.ProcessBundlesLibraryLocation, baseProcessName);
            var files = Directory.GetFiles(oldLocation, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                File.Copy(new FileInfo(file).FullName, Path.Combine(newLocation, new FileInfo(file).Name), true);
            }
            subProcRegistry.Location = newLocation;
            //open it up and update the process variables defined in the dictionary and save... tricky
            var instanceFile = Path.Combine(newLocation, "processInstance.json");
            var json = File.ReadAllText(instanceFile);
            var instance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(json);
            //Set Required process variable values to corresponding RequiredPV in parent process
            SetRequiredPVsOnSubProcessInstance(instance);
            if (subProcessProcessVariableMappings != null)
            {
                foreach (var mapping in subProcessProcessVariableMappings)
                {
                    //mapping.key is the PV name and mapping.value is the dictionary for iterations
                    instance.ProcessVariables[mapping.Key] = mapping?.Value;
                }
            }
            json = JsonConvert.SerializeObject(instance);
            File.WriteAllText(instanceFile, json);
            //start the new process
            var newProcessRunDetails = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).StartSubProcess(name, partialPathToSubProcess);
            //keep track of run_ids
            subProcRegistry.RunId = newProcessRunDetails.RunId;
            subProcRegistry.IsFinished = false;
            LogEntryWriter.LogMessage($"Spawning Sub Process with Run Id : {newProcessRunDetails.RunId}", SystemTaskVariables.ProcessRunId);
        }
      
       }
}
