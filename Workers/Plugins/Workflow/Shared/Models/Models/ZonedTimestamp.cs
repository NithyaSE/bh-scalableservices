﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class ZonedTimestamp
    {
        public string timestamp { get; set; }
        public string time_zone { get; set; }
    }
}
