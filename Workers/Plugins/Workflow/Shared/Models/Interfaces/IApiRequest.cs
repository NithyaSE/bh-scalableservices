﻿using System.Net.Http;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public interface IApiRequest
    {
        object Request { get; set; }

        HttpResponseMessage Response { get; set; }

        HttpResponseMessage HandleRequest();
    }
}
