﻿using BH.ScalableServices.Brokers.Workflow.SDK.Models;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    /// <summary>
    /// Represents a Workflow Task
    /// </summary>
    public interface IWorkflowTask
    {
        WorkflowSettings Settings { get; set; }
        SystemTaskVariables SystemTaskVariables { get; set; }
        /// <summary>
        /// Runs a task and returns a RunTaskInstance representing the status of the task
        /// </summary>
        /// <returns></returns>
        IRunTaskReturn RunTask(JObject workingSet);
    }
}
