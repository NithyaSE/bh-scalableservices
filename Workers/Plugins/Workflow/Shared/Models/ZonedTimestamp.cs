﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models
{
    public class ZonedTimestamp
    {
        public string timestamp { get; set; }
        public string time_zone { get; set; }
    }
}
