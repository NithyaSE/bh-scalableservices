﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Shared.PluginRouter;
using BH.ScalableServices.Brokers.Workflow.SDK;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Shared.CustomExceptions;
using BH.ScalableServices.Workers.Shared.Helpers;
using Shared.SerilogsLogging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Shared.ProcessHelper;
using BH.Server.Scheduler.SDK.Core;
using BH.Server.Scheduler.Models;
using NCalc;

namespace BH.ScalableServices.Workers.Plugins.Workflow
{
    public class WorkflowRunner
    {
        public IWorkflowProcessInstance WorkflowProcessInstance { get; set; }
        public IWorkflowTaskInstance CurrentWorkflowTaskInstance { get; set; }
        public static string TasksLocation { get; set; }
        public IPluginRouter PluginRouter { get; set; }
        public IWorkflowServiceClient WfsClient { get; set; }
        public IProcessRunsRepository ProcessRunsRepository { get; set; }
        private WorkflowSettings _settings { get; set; }

        public WorkflowRunner(IWorkflowServiceClient wfsClient, WorkflowSettings settings,
            IProcessRunsRepository processRunsRepo, IPluginRouter pluginLoader)
        {
            WfsClient = wfsClient;
            _settings = settings;
            ProcessRunsRepository = processRunsRepo;
            PluginRouter = pluginLoader;
        }

        public void RunProcess()
        {
            Log.Logger.AddMethodName().Information("Summary='Running process {ProcName}, RunId {RunId}'",
                WorkflowProcessInstance?.Name, WorkflowProcessInstance?.RunId);
            var processPath = Path.Combine(_settings.ProcessRunsLocation, WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Debug("Summary='Process path {ProcPath}'", processPath);
            WorkflowProcessInstance.ProcessVariables["$RunId"] = WorkflowProcessInstance.RunId;

            EvaluateProcessVariables();
            
            if (WorkflowProcessInstance.ProcessVariables.All(x => x.Key != "$current_task_execution_order_index"))
            {
                WorkflowProcessInstance.ProcessVariables.Add("$current_task_execution_order_index", 0);
                WorkflowProcessInstance.ProcessVariables.Add("$current_error_path_task_execution_order_index_index", 0);
            }
            var currentPathIndexString = WorkflowProcessInstance.ProcessVariables
                .FirstOrDefault(x => x.Key == "$current_task_execution_order_index").Value.ToString();
            var currentPathErrorIndexString = WorkflowProcessInstance.ProcessVariables
                .FirstOrDefault(x => x.Key == "$current_error_path_task_execution_order_index_index").Value.ToString();
            //temp logging... or maybe permanent
            Log.Logger.AddMethodName()
                .Information(
                    "Summary='Current task execution order index {Val} and error execution order index {ErrVal}'",
                    currentPathIndexString, currentPathErrorIndexString);
            var index = int.Parse(currentPathIndexString);
            var errorIndex = int.Parse(currentPathErrorIndexString);
            //Scan Computed PVs
            string timezone = null;
            if (!WorkflowProcessInstance.ProcessVariables.ContainsKey("!ReferenceTimeZone") || string.IsNullOrEmpty(
                    timezone =
                        WorkflowProcessInstance.ProcessVariables["!ReferenceTimeZone"]?.ToString()))
            {
                LogEntryWriter.LogMessage(
                    "No reference timezone provided. Defaulting to '(UTC) Coordinated Universal Time'",
                    WorkflowProcessInstance.RunId);
                timezone = "(UTC) Coordinated Universal Time";
            }
            WorkflowProcessInstance.ComputeProcessVariableKeywords(timezone);
            LogProcessVariables();
            if (errorIndex != 0)
            {
                RunWarningPath(errorIndex);
                return;
            }
            try
            {
                RunNormalPath(index);
                WfsClient.PostProcessOutputs(WorkflowProcessInstance);
            }
            catch (ExitApplicationCustomException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName()
                    .Error("Summary='Error in Workflow Process {@ProcName},Running Error Path' Details='{@Ex}'",
                        WorkflowProcessInstance.Name, ex);
                LogEntryWriter.LogMessage($"Error Occured - {ex.Message}. Running Warning Path.",
                    WorkflowProcessInstance.RunId);
                StatusUpdater.SetIsWarning(WorkflowProcessInstance.RunId, "Workflow Service Worker", $"{ex}");
                RunWarningPath(0);
            }
            Log.Logger.AddMethodName().Information("Summary='Finished running process {ProcName}, RunId {RunId}'",
                WorkflowProcessInstance?.Name, WorkflowProcessInstance?.RunId);
        }

        private void EvaluateProcessVariables()
        {

            var parameters = new Dictionary<string, object>();

            var results = new Dictionary<string, object>();

            foreach (var process in WorkflowProcessInstance.ProcessVariables)
            {
                //needs to stay on object if calculated
                if (process.Value.ToString().Trim().StartsWith("(") && process.Value.ToString().Trim().EndsWith(")"))
                {
                    try
                    {
                        var result = CalculateValueIfCalculated(process.Value.ToString().Trim(), parameters);
                        
                        results.Add(process.Key, result);
                        parameters.Add(process.Key, result.ToString());
                    }
                    catch (NCalc.EvaluationException exc)
                    {
                        var message = $"Could not evaluate Process Variable {process.Key} with value {process.Value}.";
                        throw new Exception(message, exc);
                    }
                }
                else
                {
                    parameters.Add(process.Key, process.Value.ToString());
                }
                
            }

            //can't change the dictionary while looping through it.
            foreach (var result in results)
            {
                WorkflowProcessInstance.ProcessVariables[result.Key] = result.Value;
            }

            /*
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            var results = new Dictionary<string, object>();

            foreach (var process in WorkflowProcessInstance.ProcessVariables)
            {

                var stringValue = process.Value.ToString().Trim();
                if (stringValue.StartsWith("(") && stringValue.EndsWith(")"))
                {
                    try
                    {
                        var evaluation = new Expression(stringValue.Substring(1, stringValue.Length - 2));
                        foreach (var param in parameters)
                        {
                            var tempVal = 0.0;
                            var isNumVal = double.TryParse(param.Value, out tempVal);

                            evaluation.Parameters.Add(param.Key, isNumVal
                                            ? new Expression(param.Value, EvaluateOptions.IgnoreCase)
                                            : new Expression("'" + param.Value.Replace("\'", "\\\'") + "'", EvaluateOptions.IgnoreCase));
                        }
                        var result = evaluation.Evaluate();

                        results.Add(process.Key, result);
                        parameters.Add(process.Key, result.ToString());
                    }
                    catch (NCalc.EvaluationException exc)
                    {
                        var message = $"Could not evaluate Process Variable {process.Key} with value {process.Value}.";
                        throw new Exception(message, exc);
                    }
                }
                else
                {
                    parameters.Add(process.Key, process.Value.ToString());
                }
            }
            
            //can't change the dictionary while looping through it.
            foreach (var result in results)
            {
                WorkflowProcessInstance.ProcessVariables[result.Key] = result.Value;
            }

    */
        }

        private void RunNormalPath(int index)
        {
            RunTasks(WorkflowProcessInstance.TaskExecutionOrder, index, "$current_task_execution_order_index");
        }

        private void RunWarningPath(int errorIndex)
        {
            try
            {
                RunTasks(WorkflowProcessInstance.OnErrorExecutionOrder, errorIndex,
                    "$current_error_path_task_execution_order_index_index");
            }
            catch (ExitApplicationCustomException)
            {
                throw;
            }
            catch (Exception exp)
            {
                Console.WriteLine(
                    $@"{DateTime.Now:s}-{
                            WorkflowProcessInstance.Name
                        }- Failed to run the warning path with exception - {exp.Message}");
                throw new MessageProcessingFailedCustomException(
                    $"Failed to run the warning path with exception - {exp.Message}", exp);
            }
        }

        private void PauseProcess(PauseRunTaskReturn finishRunTaskReturn, string currentTaskFileName)
        {
            Log.Logger.AddMethodName().Information("Summary='Pausing Workflow Process {@ProcName}'",
                WorkflowProcessInstance.Name);
            WorkflowProcessInstance.ProcessVariables["$resume_token"] = finishRunTaskReturn.ResumeToken;
            DehydrateWfsProcessAndTaskInstance(currentTaskFileName);
        }

        private void Dehydrate(DehydrateRunTaskReturn dehydrateRunTaskReturn, string currentTaskFileName)
        {
            DehydrateWfsProcessAndTaskInstance(currentTaskFileName);
            ScheduleProcessToRunAfterInterval(dehydrateRunTaskReturn.CheckInterval);
        }

        private void DehydrateWfsProcessAndTaskInstance(string currentTaskFileName)
        {
            ProcessRunsRepository.DehydrateTempTask(WorkflowProcessInstance.RunId, currentTaskFileName,
                CurrentWorkflowTaskInstance);
            LogEntryWriter.LogMessage($"Dehydrating task : {CurrentWorkflowTaskInstance.Name}",
                WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Information("Summary='Dehydrating Workflow Process {@ProcName}'",
                WorkflowProcessInstance.Name);
            StatusUpdater.SetIsDehydrated(WorkflowProcessInstance.RunId, "Workflow Service Worker",
                $"Dehydrating process - {WorkflowProcessInstance.Name}");
            ProcessRunsRepository.SaveProcessInstance(WorkflowProcessInstance);
        }

        private void ScheduleProcessToRunAfterInterval(string checkInterval)
        {
            var targetUrl = $"{_settings.WorkflowSchedulerTargetUrl.Replace("process_instance_name=", "")}";
            var wfsCreds = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).Header;
            var targetHeaders =
                new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Authorization", wfsCreds.Value)
                };
            var creds = _settings.WorkflowSchedulerCreds;

            var client = new SchedulerClient(_settings.WorkflowSchedulerBaseUri, creds);

            var schedule = new ScheduleInputs
            {
                BaseTime = DateTime.UtcNow,
                Uri = targetUrl,
                Parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("run_id", WorkflowProcessInstance.RunId)
                },
                Body = "",
                Headers = targetHeaders,
                Interval = checkInterval

            };
            try
            {
                var instance = client.ScheduleHTTPRequest(schedule);

                LogEntryWriter.LogMessage(
                    $"\nScheduler successfully accepted schedule. \n Instance Details: {JsonConvert.SerializeObject(instance)}",
                    WorkflowProcessInstance.RunId);
            }
            catch (Exception ex)
            {
                LogEntryWriter.LogMessage(
                    $"\nSchedler failed to accept schedule. \n Input Details: {JsonConvert.SerializeObject(schedule)}",
                    WorkflowProcessInstance.RunId);
                throw;
            }

        }

        private void RunTasks(IList<string> taskExecutionOrder, int taskExecutionIndex, string indexPVName)
        {
            if (taskExecutionOrder == null) return;
            foreach (var nextTaskName in taskExecutionOrder.Skip(taskExecutionIndex))
            {
                var runTaskReturn = RunTask(nextTaskName);
                if (runTaskReturn is DehydrateRunTaskReturn || runTaskReturn is PauseRunTaskReturn)
                {
                    return;
                }
                WorkflowProcessInstance.ProcessVariables[indexPVName] = ++taskExecutionIndex;
            }
        }

        private IRunTaskReturn RunTask(string taskName)
        {
            string currentTaskFileName = null;
            InitializeCurrentWorkflowTaskInstance(taskName, out currentTaskFileName);
            try
            {
                if (!IsCurrentWfsTaskInstanceActive())
                {
                    LogEntryWriter.LogMessage(
                        $"Task instance {taskName} has property is_active set to false. Skipping the task instance.",
                        WorkflowProcessInstance.RunId);
                    CleanUpAfterTaskFinish(currentTaskFileName);
                    return new FinishRunTaskReturn();
                }
                var runTaskReturn = RunCurrentTask();
                LogProcessVariables();
                if (runTaskReturn is DehydrateRunTaskReturn)
                {
                    var dehydrateRunTaskReturn = (DehydrateRunTaskReturn)runTaskReturn;
                    CurrentWorkflowTaskInstance.WorkingSet = dehydrateRunTaskReturn.WorkingSet;
                    Dehydrate(dehydrateRunTaskReturn, currentTaskFileName);
                }
                else if (runTaskReturn is PauseRunTaskReturn)
                {
                    var pauseRunTaskReturn = (PauseRunTaskReturn)runTaskReturn;
                    CurrentWorkflowTaskInstance.WorkingSet = pauseRunTaskReturn.WorkingSet;
                    PauseProcess(pauseRunTaskReturn, currentTaskFileName);
                }
                else if (runTaskReturn is FinishRunTaskReturn)
                {
                    try
                    {
                        var finishRunTaskReturn = (FinishRunTaskReturn)runTaskReturn;
                        LogEntryWriter.LogObject(finishRunTaskReturn.WorkingSet,
                            $"The working set for {CurrentWorkflowTaskInstance.Name}", WorkflowProcessInstance.RunId);
                        MapCurrentTaskWorkingSetToPvs(finishRunTaskReturn);
                        LogProcessVariables();
                    }
                    finally
                    {
                        CleanUpAfterTaskFinish(currentTaskFileName);
                    }
                }

                return runTaskReturn;
            }
            catch
            {
                CleanUpAfterTaskFinish(currentTaskFileName);
                throw;
            }

        }

        private void CleanUpAfterTaskFinish(string currentTaskFileName)
        {
            ProcessRunsRepository.RemoveTempTaskFile(WorkflowProcessInstance.RunId, currentTaskFileName);
            WorkflowProcessInstance.ProcessVariables.Remove("$current_task_file_name");
        }
        private void InitializeCurrentWorkflowTaskInstance(string taskName, out string currentTaskFileName)
        {
            CurrentWorkflowTaskInstance = null;
            currentTaskFileName = "";
            bool isRehydrated = false;
            if (WorkflowProcessInstance.ProcessVariables.ContainsKey("$current_task_file_name"))
            {
                currentTaskFileName = WorkflowProcessInstance.ProcessVariables["$current_task_file_name"]?.ToString();
                CurrentWorkflowTaskInstance =
                    (WorkflowTaskInstance)ProcessRunsRepository.RehydrateTempTask(WorkflowProcessInstance.RunId,
                        currentTaskFileName);
                isRehydrated = true;
            }
            else
            {
                // new task first run
                CurrentWorkflowTaskInstance = (WorkflowTaskInstance)GetTaskInstance(taskName);
                currentTaskFileName =
                    ProcessRunsRepository.CreateTempTaskInstance(CurrentWorkflowTaskInstance,
                        WorkflowProcessInstance.RunId);
                WorkflowProcessInstance.ProcessVariables["$current_task_file_name"] =
                    currentTaskFileName; // will be saved when des
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs,
                    $"The Working Set Input mappings to process variables for {CurrentWorkflowTaskInstance.Name}",
                    WorkflowProcessInstance.RunId);
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSetOutputMappingsToPvs,
                    $"The Working Set Output mappings to process variables for {CurrentWorkflowTaskInstance.Name}",
                    WorkflowProcessInstance.RunId);
            }
            InitializeSystemTaskVariablesForCurrentWfsTask(isRehydrated);
        }

        private void InitializeSystemTaskVariablesForCurrentWfsTask(bool isRehydrated)
        {
            var systemTvs = new JObject
            {
                ["$run_id"] = WorkflowProcessInstance.RunId,
                ["$is_rehydrated"] = isRehydrated,
                ["$workflow_task_instance_name"] = CurrentWorkflowTaskInstance.Name
            };
            foreach (var requiredPv in WorkflowProcessInstance.ProcessVariables.Where(pv => pv.Key.StartsWith("!")))
            {
                systemTvs[requiredPv.Key] = JToken.FromObject(requiredPv.Value);
            }
            CurrentWorkflowTaskInstance.SystemTaskVariables = systemTvs;
        }

        private void MapCurrentTaskWorkingSetToPvs(FinishRunTaskReturn finishRunTaskReturn)
        {
            if (finishRunTaskReturn.WorkingSet == null) return;
            //Map the top level output tvs to pvs
            foreach (var mapping in CurrentWorkflowTaskInstance.WorkingSetOutputMappingsToPvs)
            {
                var outputPropertyName = mapping.Key;
                var processVariableName = mapping.Value;
                if (string.IsNullOrEmpty(processVariableName)) continue;
                if (!WorkflowProcessInstance.ProcessVariables.ContainsKey(processVariableName))
                    throw new Exception(
                        $"The process variable {processVariableName} mapped to working set property {outputPropertyName} does not exist in the list of process variables");
                if (finishRunTaskReturn.WorkingSet.ContainsKey(outputPropertyName))
                {
                    WorkflowProcessInstance.SetProcessVariable(processVariableName,
                        finishRunTaskReturn.WorkingSet[outputPropertyName]);
                }
                else
                {
                    throw new Exception(
                        $"The property {outputPropertyName} could not be found on the output task variables provided in the finish task return");
                }

            }
        }

        private IWorkflowTaskInstance GetTaskInstance(string taskName)
        {
            var taskInstance = ProcessRunsRepository.GetTaskInstanceFromRunId(WorkflowProcessInstance.RunId, taskName);
            if (taskInstance == null)
                throw new Exception(
                    $"The task instance with name {taskName} could not be found in the Runs directory");
            return taskInstance; // found and not null
        }

        private bool IsCurrentWfsTaskInstanceActive()
        {
            return CurrentWorkflowTaskInstance.IsActive.IsMapped
                ? GetMappedVariable(CurrentWorkflowTaskInstance.IsActive.ProcessVariable, typeof(bool))
                : CurrentWorkflowTaskInstance.IsActive.Value;
        }

        private void LogProcessVariables()
        {
            var jsonPVs = JsonConvert.SerializeObject(WorkflowProcessInstance.ProcessVariables);
            LogEntryWriter.LogMessage($"Process Variables: {jsonPVs}", WorkflowProcessInstance.RunId);
        }

        public IRunTaskReturn RunCurrentTask()
        {
            IRunTaskReturn runTaskReturn;
            LogEntryWriter.LogMessage(CurrentWorkflowTaskInstance.Name + " (Task) : Started",
                WorkflowProcessInstance.RunId);
            Console.WriteLine(
                $@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{
                        CurrentWorkflowTaskInstance.Name
                    } - Started");
            Log.Logger.AddMethodName().Information("Summary='{TaskInstanceName},{TaskDllName} Started'",
                CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            try
            {
                //Run Stinky code to kill all rogue excel instaces
                //TODO: This issue could use some investigation for the root cause of rogue excels and this code could be cleaned up.
                ProcessHelper.TryKillAllProcessInstancesByName("Excel");
                ProcessHelper.TryKillAllProcessInstancesByName("WinWord");
                var workflowTask = GetWorkflowTask();
                workflowTask.SetSystemTaskVariablesJObject(CurrentWorkflowTaskInstance.SystemTaskVariables);
                InitializeCurrentWfsTaskWorkingSet();
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSet,
                    $"The working set task variables for {CurrentWorkflowTaskInstance.Name}",
                    WorkflowProcessInstance.RunId);
                runTaskReturn = workflowTask.RunTask(CurrentWorkflowTaskInstance.WorkingSet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    $@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{
                            CurrentWorkflowTaskInstance.Name
                        } - Failed");
                LogEntryWriter.LogMessage(ex.ToString(), WorkflowProcessInstance.RunId);
                Log.Logger.AddMethodName()
                    .Information("Summary='{TaskInstanceName},{TaskDllName} Failed' Details='{@Ex}'",
                        CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName, ex);
                throw;
            }
            Console.WriteLine(
                $@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{
                        CurrentWorkflowTaskInstance.Name
                    } - Completed");
            LogEntryWriter.LogMessage(CurrentWorkflowTaskInstance.Name + " (Task) : Finished",
                WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Information("Summary='{@TaskInstanceName},{TaskDllName} Finished'",
                CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            return runTaskReturn;
        }

        #region Logging Methods

        /// <summary>
        /// Writes to the log in the process instances folder.
        /// </summary>
        /// <param name="msg">Message to be logged</param>
        /// <param name="type"></param>

        #endregion
        public WorkflowTask GetWorkflowTask()
        {
            WorkflowTask task = null;
            Log.Logger.AddMethodName()
                .Debug(
                    "Summary='Attempting to load the plugin dlls for task {@taskName}. Task Dll Name - {@taskDllName}'",
                    CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            //try to load dlls 
            try
            {
                Log.Logger.AddMethodName()
                    .Debug("Summary='Loading the plugin dlls for task {@taskName}. Task Dll Name - {@taskDllName}'",
                        CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
                task = PluginRouter.GetPlugins<WorkflowTask>(CurrentWorkflowTaskInstance.TaskDllName,
                    new List<string>
                    {
                        AppDomain.CurrentDomain.BaseDirectory,
                        TasksLocation
                    }).FirstOrDefault();
                if (task == null)
                    throw new Exception("The task dll lookup returned null.");
            }
            catch (Exception exp)
            {
                throw new Exception(
                    $"Summary='Failed to load the plugin dlls for task {CurrentWorkflowTaskInstance.Name}. Task Dll Name - {CurrentWorkflowTaskInstance.TaskDllName}' with Exception {@exp}",
                    exp);
            }
            Log.Logger.AddMethodName()
                .Debug("Summary='Plugin dlls loaded successfully for task {@taskName}. Task Dll Name - {@taskDllName}'",
                    CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            task.Settings = _settings;
            return task;
        }

        private void InitializeCurrentWfsTaskWorkingSet()
        {
            if (bool.Parse(CurrentWorkflowTaskInstance.SystemTaskVariables["$is_rehydrated"].ToString())) return;
            if (CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs == null) return;
            var workingSet = CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs.DeepClone();
            InitializeWorkingSet(workingSet);
            CurrentWorkflowTaskInstance.WorkingSet = workingSet.ToObject<JObject>();
        }

        public dynamic GetMappedVariable(string processVariableName, Type t)
        {
            object value = null;
            if (t.IsValueType)
                value = Activator.CreateInstance(t);
            var methodInfo = typeof(WorkflowProcessInstance).GetMethod("GetVariable");
            if (methodInfo != null)
                value = methodInfo.MakeGenericMethod(t).Invoke(
                    (WorkflowProcessInstance)WorkflowProcessInstance, new object[]
                    {
                        processVariableName
                    });
            return value;
        }
        /// <summary>
        /// 1. This code reads a working set with bh_mappable property types and converts it 
        /// to an object with all properties mapped.
        /// 2. This method only allows array elements to be top level bhmappable objects, all other bhmappable objects must be 
        /// a value of a property.
        /// </summary>
        /// <param name="field"></param>
        private void InitializeWorkingSet(dynamic field)
        {
            if (field == null) return;
            var fieldJArray = field as JArray;
            if (fieldJArray != null) // JArray
            {
                fieldJArray = (JArray)field;
                var itemsToDelete = new List<JToken>();
                var itemsToAdd = new List<JToken>();
                foreach (var arrayItem in fieldJArray)
                {
                    // Check if array item is mappable. (a top level object is allowed to be bhmappable only if it is in an array)
                    BHWorkflowTaskMappableVariable<JToken> arrayObjAsBhMappable = null;
                    if (arrayItem is JObject && (arrayObjAsBhMappable = ConvertObjectToBhMappable((JObject)arrayItem)) != null)
                    {
                        dynamic arrayObjValue;
                        if (arrayObjAsBhMappable.IsMapped)
                        {
                            var mappedVal = WorkflowProcessInstance.GetProcessVariable(
                                arrayObjAsBhMappable.ProcessVariable);
                            arrayObjValue = JToken.FromObject(mappedVal); // move the value up
                        }
                        else
                        {
                            arrayObjValue = arrayObjAsBhMappable.Value; // move the value up
                        }
                        itemsToDelete.Add(arrayItem); // mark the current object for deletion
                        itemsToAdd.Add(arrayObjValue); // mark the object value as new object to be added to the array.
                        InitializeWorkingSet(arrayObjValue); // recursively process the value of new object to be added to array
                    }
                    else
                    {
                        InitializeWorkingSet(arrayItem);
                    }
                }
                itemsToDelete.ForEach(item => fieldJArray.Remove(item));
                itemsToAdd.ForEach(item => fieldJArray.Add(item));
            }
            else
            {
                var fieldJObj = field as JObject;
                if (fieldJObj == null) return; // Not an object and not an array. Value needs to be returned
                fieldJObj = (JObject)field;
                foreach (var jProperty in fieldJObj.Properties())
                {
                    // check if property is an object and contains is_mapped 
                    var propertyVal = jProperty.Value;
                    if (!(propertyVal is JArray) && !(propertyVal is JObject))
                    {
                        continue;
                    }
                    var propertyValAsJObj = propertyVal as JObject;
                    if (propertyValAsJObj != null)
                    {
                        //check if the jobject has is_mapped
                        BHWorkflowTaskMappableVariable<JToken> propertyValAsMappableProp = null;
                        if ((propertyValAsMappableProp = ConvertObjectToBhMappable(propertyValAsJObj)) != null)
                        {
                            if (propertyValAsMappableProp.IsMapped)
                            {
                                var mappedVal = WorkflowProcessInstance.GetProcessVariable(
                                    propertyValAsMappableProp.ProcessVariable);
                                jProperty.Value = JToken.FromObject(mappedVal); // move the value up
                            }
                            else
                            {
                                //mappable object that isn't mapped.  Evaluate
                                //jProperty.Value = propertyValAsMappableProp.Value; // move the value up
                                //jProperty.Value = EvaluateTaskVariable(propertyValAsMappableProp.Value); // move the value up
                                // might be original, so needs to be put as jtoken
                                var stringValue = propertyValAsMappableProp.Value.ToString().Trim();
                                if (stringValue.StartsWith("(") && stringValue.EndsWith(")"))
                                // might be object, so needs to be put as jtoken
                                {
                                    var result = CalculateValueIfCalculated(propertyValAsMappableProp.Value.ToString(), WorkflowProcessInstance.ProcessVariables);

                                    try
                                    {
                                        // numerical values and json converts fine.  strings do not.  if there's an error, try as a string
                                        jProperty.Value = JsonConvert.DeserializeObject<JToken>(result);
                                    }
                                    //else // needs to know it's a string
                                    catch
                                    {
                                        jProperty.Value = JsonConvert.DeserializeObject<JToken>($"\"{result}\"");
                                    }
                                }
                                else
                                    jProperty.Value = propertyValAsMappableProp.Value; // move the value up
                            }
                            InitializeWorkingSet(jProperty.Value);
                        }
                        else continue;
                    }
                    else // JArray
                    {
                        InitializeWorkingSet(jProperty.Value); // if null then could be JArray
                    }
                }
            }
        }

        /*
        private JToken EvaluateTaskVariable(JToken value)
        {
            var stringValue = value.ToString().Trim();
            if (stringValue.StartsWith("(") && stringValue.EndsWith(")"))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                foreach (var process in WorkflowProcessInstance.ProcessVariables)
                {

                    parameters.Add(process.Key, process.Value.ToString());
                }

                var evaluation = new Expression(stringValue.Substring(1, stringValue.Length - 2));
                foreach (var param in parameters)
                {
                    var tempVal = 0.0;
                    var isNumVal = double.TryParse(param.Value, out tempVal);

                    evaluation.Parameters.Add(param.Key, isNumVal
                                    ? new Expression(param.Value, EvaluateOptions.IgnoreCase)
                                    : new Expression("'" + param.Value.Replace("\'", "\\\'") + "'", EvaluateOptions.IgnoreCase));
                }
                var result = evaluation.Evaluate();
                return JToken.FromObject(result.ToString()); // wrong

            }
            else
                return value;
        }*/

        private BHWorkflowTaskMappableVariable<JToken> ConvertObjectToBhMappable(JObject jobject)
        {
            // check if the jobject has is_mapped
            if (!jobject.ContainsKey("is_mapped")) return null;
            BHWorkflowTaskMappableVariable<JToken> objectAsBhMappable = null;
            try
            {
                objectAsBhMappable = jobject.ToObject<BHWorkflowTaskMappableVariable<JToken>>();
            }
            catch
            {
                return null;
            }
            return objectAsBhMappable;
        }


        private string CalculateValueIfCalculated(string value, Dictionary<string,object> parameters)
        {
            var stringValue = value.Trim();
            if (stringValue.StartsWith("(") && stringValue.EndsWith(")"))
            {
                var evaluation = new Expression(stringValue.Substring(1, stringValue.Length - 2));
                foreach (var param in parameters)
                {
                    var tempVal = 0.0;
                    var isNumVal = double.TryParse(param.Value.ToString(), out tempVal);

                    evaluation.Parameters.Add(param.Key, isNumVal
                                    ? new Expression(param.Value.ToString(), EvaluateOptions.IgnoreCase)
                                    : new Expression("'" + param.Value.ToString().Replace("\'", "\\\'") + "'", EvaluateOptions.IgnoreCase));
                }
                var result = evaluation.Evaluate();
                return result.ToString();

            }
            else
                return value;
        }
    }
}