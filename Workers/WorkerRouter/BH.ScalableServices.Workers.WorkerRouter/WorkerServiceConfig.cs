﻿using BH.ScalableServices.Brokers.SDKBase;
using Topshelf;
using Shared.PluginRouter;

namespace BH.ScalableServices.Worker.WorkerRouter
{
    /// <summary>
    /// Class to configure the service
    /// </summary>
    public static class WorkerServiceConfig<T> where T : Worker,new()
    {
        public static void Configure(string serviceDescription,string serviceDisplayName,string serviceName,IScalableServiceWebClient webClient,IPluginRouter pluginRouter=null)
        {
            HostFactory.Run(configure =>
            {
                configure.Service<WorkerServiceRunner<T>>(service =>
                {
                    service.ConstructUsing(s => new WorkerServiceRunner<T>(webClient, pluginRouter)
                    {
                        ServiceName = serviceName
                    });
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetServiceName(serviceName);
                configure.SetDisplayName(serviceDisplayName);
                configure.SetDescription(serviceDescription);
            });
        }
    }
}


