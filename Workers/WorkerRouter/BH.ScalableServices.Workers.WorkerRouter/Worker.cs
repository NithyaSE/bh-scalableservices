﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon.SQS.Model;
using Shared.AmazonSQS;
using Shared.PluginRouter;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Enums;
using BH.ScalableServices.Brokers.Shared.Models;
using BH.ScalableServices.Workers.Shared.CustomExceptions;
using BH.ScalableServices.Workers.Shared.Helpers;
using BH.ScalableServices.Workers.Shared.Interfaces;
using Shared.SerilogsLogging;
using Shared.Tools;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace BH.ScalableServices.Worker.WorkerRouter
{
    /// <summary>
    /// Main Worker class that loads the plugin and reads messages from sqs.
    /// </summary>
    public class Worker
    {
        // If signal = 1 initially then wait on untill reset else keep running until signalled
        protected readonly ManualResetEvent StopEvent = new ManualResetEvent(false);
        protected readonly ManualResetEvent StopPersistMessageThreadEvent = new ManualResetEvent(false);
        public IScalableServiceWebClient ServiceWebClient { get; set; }
        public IPluginRouter PluginRouter { get; set; }
        public string PluginLocation => ConfigurationManager.AppSettings["PluginLocation"];
        public string ServiceName { get; set; }
        protected string QueueUrl { get; set; }
        protected string DeadLetterQueueUrl { get; set; } = ConfigurationManager.AppSettings["DeadLetterQueueURL"];
        protected int MaxQueueRcvCount;
        private AmazonSQSHelper sqsHelper;
        public string LoggingName { get; }
        public Worker()
        {
            sqsHelper = new AmazonSQSHelper();
            SetAwsDefaults();
        }
        public void Go()
        {
            StatusUpdater.ServiceWebClient = ServiceWebClient;
                //Loop until service is stopped
                while (!StopEvent.WaitOne(0))
                {
                    try
                    {

                    //To minimize the timer start time between the SQS queue and the code, read the timer value from app.config(in loop to allow hot update)
                    ICollection<Message> messages = null;
                    try
                    {
                        //Fetch message (waiting up to 20 seconds for response)
                        messages = sqsHelper.ReadMessagesFromQueue(20, 1);
                        if (messages == null || messages.Count <= 0)
                            continue;
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.AddMethodName().Error("Summary='Error occured in Worker while reading messages from SQS. Retrying to read the messages.' Details='{@Error}'", ex);
                        continue;
                    }
                    foreach (var message in messages)
                    {
                        ScalableServiceRunMessage<JToken> req;
                        StopPersistMessageThreadEvent.Reset();

                        Thread rt = new Thread(() => PersistMessage(message));
                        rt.Start();
                        try
                        {
                            Log.Logger.AddMethodName().Information("Summary='Message taken off the queue {@Queue}. Deserializing the message.' Details='Received Request {@Message}'", sqsHelper.QueueURL, message.Body);
                            req = JsonConvert.DeserializeObject<ScalableServiceRunMessage<JToken>>(message.Body);
                            // Scalable service web client initialization for worker router
                            var scalableServiceWebClient = new ScalableServiceWebClient();
                            scalableServiceWebClient.Setup(ConfigurationManager.AppSettings[$"{req.WorkerType}ScalableApiEndPoint"],
                                ConfigurationManager.AppSettings[$"{req.WorkerType}ScalableApiUsername"],
                                ConfigurationManager.AppSettings[$"{req.WorkerType}ScalableApiPassword"]);
                            StatusUpdater.ServiceWebClient = scalableServiceWebClient; 
                        }
                        catch (Exception exp)
                        {
                            // desrialization exception
                            Log.Logger.AddMethodName().Error("Summary='Error occured in the worker while deserializing the request message. Putting the message back on the queue.' Details='{@exp}'", exp);
                            sqsHelper.SetVisibilityTimeoutOnMessage(message, 0);
                            continue;
                        }
                        var runId = req?.RunId;
                        var codeBase = typeof(Worker).Assembly.CodeBase;
                        var uri = new UriBuilder(codeBase);
                        var newAppDomain = AppDomain.CreateDomain("newAppDomain", null, Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path)), null, false);
                        newAppDomain.SetData("MessageBody", message.Body);
                        newAppDomain.SetData("LoggingName",LoggingName);
                        try
                        {
                            newAppDomain.DoCallBack(() =>
                            { 
                                // all this in new app domain 
                                    // console color changes per worker type
                                    // load the worker type by message type
                                    var appDomainrequestJson = (string) AppDomain.CurrentDomain.GetData("MessageBody");
                                    var appDomainRequest = JsonConvert.DeserializeObject<ScalableServiceRunMessage<JToken>>(appDomainrequestJson);
                                    var appDomainLoggingName = (string) AppDomain.CurrentDomain.GetData("LoggingName");
                                    // Scalable service web client initialization for worker plugin inside app domain.
                                    var scalableServiceWebClient = new ScalableServiceWebClient();
                                    scalableServiceWebClient.Setup(ConfigurationManager.AppSettings[$"{appDomainRequest.WorkerType}ScalableApiEndPoint"],
                                    ConfigurationManager.AppSettings[$"{appDomainRequest.WorkerType}ScalableApiUsername"],
                                    ConfigurationManager.AppSettings[$"{appDomainRequest.WorkerType}ScalableApiPassword"]);
                                    StatusUpdater.ServiceWebClient = scalableServiceWebClient;
                                    StatusUpdater.SetIsExecuting(appDomainRequest.RunId, appDomainLoggingName);
                                    Console.ForegroundColor = appDomainRequest.WorkerType.GetConsoleColor();
                                    var pluginRouter = new PluginRouter();
                                    var pluginLocation = ConfigurationManager.AppSettings["PluginLocation"];
                                    var pluginBaseName = ConfigurationManager.AppSettings["PluginBaseName"];
                                    var pluginName = $"{pluginBaseName}.{appDomainRequest.WorkerType}";
                                    var workerPlugin = pluginRouter.GetPlugins<IWorkerPlugin>(pluginName, new[] { pluginLocation }, null).FirstOrDefault();//get worker that was built with it
                                    workerPlugin.ProcessMessage(appDomainRequest);
                                    
                            });
                            StopPersistMessageThreadEvent.Set();
                            rt.Join(); 
                            sqsHelper.DeleteMessageFromQueue(message);
                            StatusUpdater.SetSuccessfullExecution(req.RunId, LoggingName);
                        }
                        catch (TimeoutException e)
                        {
                            // In case of timeout : Queue Message - NO;Delete Message - YES;is_exception - TRUE
                            StopPersistMessageThreadEvent.Set();
                            rt.Join(); //thread stop needed?
                            SetIsExceptionStatusMoveToDeadLetterQueue(message, runId, e);
                            Log.Logger.AddMethodName().Error("Summary='Timeout Occured while processing the message.' Details='{@Error}'", e);
                        }
                        catch (MessageProcessingFailedCustomException e)
                        {
                            StopPersistMessageThreadEvent.Set();
                            rt.Join(); //thread stop needed?
                            SetIsExceptionStatusMoveToDeadLetterQueue(message, runId, e);
                            Log.Logger.AddMethodName().Error("Summary='Failed to process the message in the warning path.' Details='{@Error}'", e);
                        }
                        catch (ExitApplicationCustomException e)
                        {
                            StopPersistMessageThreadEvent.Set();
                            rt.Join(); //thread stop needed?
                            sqsHelper.SetVisibilityTimeoutOnMessage(message, 0);
                            StatusUpdater.ResetStatusOnVisible(runId, LoggingName);
                            Log.Logger.AddMethodName().Error("Summary='Exiting Application' Details='{@Error}'", e);
                            Environment.Exit(-1);
                        }
                        catch (Exception e)
                        {
                            var msg = e.GetExceptionMessage();
                            Console.WriteLine(msg);
                            //Default exception cases. Check for lives
                            var lives = GetRemainingLives(message);
                            if (lives > 0)
                            {
                                ServiceWebClient.PostLogEntry(new ScalableServiceLogEntry()
                                {
                                    Timestamp = DateTimeOffset.UtcNow,
                                    Source = LoggingName,
                                    Message = $"Error occured while processing the message '{msg}'. Total lives - '{MaxQueueRcvCount}'. Lives remaining - '{lives}'. Requeueing the message"
                                }, runId);
                                Log.Logger.AddMethodName().Error("Summary='Error occured while processing the message {msg}. Requeueing the message. {lives} lives are remaining.'. Details='{@Error}'", msg, lives, e);
                                StopPersistMessageThreadEvent.Set();
                                rt.Join(); //thread stop needed?
                                sqsHelper.SetVisibilityTimeoutOnMessage(message, 0);
                                StatusUpdater.ResetStatusOnVisible(runId, LoggingName);
                            }
                            else
                            {
                                StopPersistMessageThreadEvent.Set();
                                rt.Join(); //thread stop needed?
                                SetIsExceptionStatusMoveToDeadLetterQueue(message, runId, e);
                                Log.Logger.AddMethodName().Error("Summary='Error Occured while processing the message {msg}'. Details='{@Error}'", msg, e);
                            }
                        }
                        finally
                        {
                            Log.Logger.AddMethodName().Information("Summary='Unloading the app domain.'");
                            AppDomain.Unload(newAppDomain);
                        }
                    }
                }
                catch (Exception ex)
                {
                        Log.Logger.AddMethodName().Fatal("Summary='Fatal Error in Worker' Details='{@Error}'", ex);
                }
            }
            
        }
        public virtual void CancelHandler()
        {
            throw new Exception();
        }
        public void RequestStop()
        {
            StopEvent.Set();
        }
        private void SetAwsDefaults()
        {
            QueueUrl = ConfigurationManager.AppSettings["QueueURL"];
            var attributes = sqsHelper.GetQueueAttributes(new List<string> { "RedrivePolicy" });
            var redrivePolicyStr = attributes?["RedrivePolicy"];
            if (string.IsNullOrEmpty(redrivePolicyStr)) return;
            var redrivePolicyJson = JsonConvert.DeserializeObject<JToken>(redrivePolicyStr);
            MaxQueueRcvCount = redrivePolicyJson["maxReceiveCount"].Value<int>();
            if (!string.IsNullOrEmpty(DeadLetterQueueUrl)) return;
            var deadLetterQueueName = redrivePolicyJson["deadLetterTargetArn"]?.Value<string>()?.Split(':')?.Last();
            if (string.IsNullOrEmpty(deadLetterQueueName)) return;
            var index = QueueUrl.LastIndexOf("/", StringComparison.Ordinal);
            if (index > 0)
                DeadLetterQueueUrl = $"{QueueUrl.Substring(0, index + 1)}{deadLetterQueueName}";
        }
        /// <summary>
        /// Sets the is_exception = true on the run status and moves the message to the dead letter queue
        /// </summary>
        /// <param name="message">The sqs message</param>
        /// <param name="runId">The run ID</param>
        /// <param name="exp">The exception that occured</param>
        private void SetIsExceptionStatusMoveToDeadLetterQueue(Message message, string runId, Exception exp)
        {
            var expMsg = "";
            expMsg = exp is AggregateException ? ((AggregateException)exp).Flatten().Message : exp.Message;
            StatusUpdater.SetIsException(runId, LoggingName, expMsg);
            sqsHelper.DeleteMessageFromQueue(message);
            sqsHelper.WriteMessageToQueue(message.Body, DeadLetterQueueUrl);
        }
        /// <summary>
        /// Gets the remaining lives for the message
        /// </summary>
        /// <param name="message">The message</param>
        /// <returns></returns>
        private int GetRemainingLives(Message message)
        {
            var approxRcvCount = (message?.Attributes!=null)&&message.Attributes.ContainsKey("ApproximateReceiveCount")? message.Attributes["ApproximateReceiveCount"]:null;
            if (string.IsNullOrEmpty(approxRcvCount)) return 0;
            return MaxQueueRcvCount - int.Parse(approxRcvCount);
        }

        private void PersistMessage(Message message) // static?
        {
            Dictionary<string, string> attributes = sqsHelper.GetQueueAttributes(new List<string> { "VisibilityTimeout" });
            var visibilityTimeoutString = attributes?["VisibilityTimeout"];
            var visibilityTimeout = int.Parse(visibilityTimeoutString);

            while (!StopPersistMessageThreadEvent.WaitOne(visibilityTimeout / 2 * 1000)) // assumes milleseconds.  visibility timeout in seconds.  wait on in milliseconds
            {
                sqsHelper.SetVisibilityTimeoutOnMessage(message, visibilityTimeout);
            }
        }
    }
}