﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Shared.Models
{
    public class ScalableServiceStatus
    {
        [JsonProperty("is_queued")]
        public bool? IsQueued { get; set; }
        [JsonProperty("is_executing")]
        public bool? IsExecuting { get; set; }
        [JsonProperty("is_warning")]
        public bool? IsWarning { get; set; }
        [JsonProperty("is_exception")]
        public bool? IsException { get; set; }
        [JsonProperty("is_dehydrated")]
        public bool? IsDehydrated { get; set; }
    }
}
