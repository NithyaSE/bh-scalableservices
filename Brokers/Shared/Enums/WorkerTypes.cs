using System;
using System.Runtime.Remoting.Messaging;

namespace BH.ScalableServices.Brokers.Shared.Enums
{
    public enum WorkerType
    {
        DocumentProduction,
        Workflow,
        Visualization,
        Message
    }
    public static class WorkerTypeExtensions
    {
        public static ConsoleColor GetConsoleColor(this WorkerType value)
        {
            switch (value)
            {
                case WorkerType.DocumentProduction:return ConsoleColor.Blue;
                case WorkerType.Workflow:return ConsoleColor.Green;
                case WorkerType.Visualization: return ConsoleColor.Magenta;
                case WorkerType.Message:return  ConsoleColor.Yellow;
                default: return ConsoleColor.White;
            }
        }
    }
}
