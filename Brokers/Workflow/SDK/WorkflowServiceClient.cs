﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Shared.WebClient;
using Shared.WebClient.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.SDK
{
    public class WorkflowServiceClient : ScalableServiceWebClient, IWorkflowServiceClient
    {
        public IWorkflowProcessInstance GetProcessInstance(string processInstanceName)
        {
            var request = new HttpRequest(HttpVerb.Get);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Resource", "processInstances"));
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "process_instance_name",
                Value = processInstanceName
            });
            var processInstance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
            //Its possible that the we are talking to the incorrect workflow service (maybe wrong in the .config)
            //then the process wont exist.
            if (processInstance == null)
            {
                throw new Exception(
                    "Process Instance could not be found.  Make sure the settings are correct for communicating with the Workflow Service.");
            }
            return processInstance;
        }

        public IWorkflowTaskInstance GetTaskInstance(string processInstanceName, string taskInstanceName)
        {
            var request = new HttpRequest(HttpVerb.Get);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Resource", "taskInstances"));
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "process_instance_name",
                Value = processInstanceName
            });
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "task_instance_name",
                Value = taskInstanceName
            });
            var taskInstance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
            //Its possible that the we are talking to the incorrect workflow service (maybe wrong in the .config)
            //then the process wont exist.
            if (taskInstance == null)
            {
                throw new Exception(
                    "Task Instance could not be found.  Make sure the settings are correct for communicating with the Workflow Service.");
            }
            return taskInstance;
        }

        public List<IWorkflowTaskInstance> GetTaskInstances(IWorkflowProcessInstance processInstance)
        {
            var workflowTaskInstances = processInstance.TaskExecutionOrder.Select(task => GetTaskInstance(processInstance.Name, task)).ToList();
            workflowTaskInstances.AddRange(processInstance.OnErrorExecutionOrder.Select(task => GetTaskInstance(processInstance.Name, task)));
            return workflowTaskInstances;
        }

        public void PostProcessOutputs(IWorkflowProcessInstance processInstance)
        {
            var outputs = processInstance.SetOutputs();
            var request = new HttpRequest(HttpVerb.Post);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Resource", "outputs"));
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "run_id",
                Value = processInstance.RunId
            });
            request.Body = new StringContent(JsonConvert.SerializeObject(outputs));
            HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request));

        }
        public ScalableServiceRunDetails StartSubProcess(string processName, string partialPath)
        {
            var request = new HttpRequest(HttpVerb.Post);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Resource", "startsubprocess"));
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "process_instance_name",
                Value = processName
            });
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "partial_path",
                Value = partialPath
            });
           return JsonConvert.DeserializeObject<ScalableServiceRunDetails>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
        }
        public Dictionary<string, object> GetProcessOutputs(string runId)
        {
            var request = new HttpRequest(HttpVerb.Get);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Resource", "outputs"));
            request.ParameterCollection.Add(new HttpParameter
            {
                ParameterName = "run_id",
                Value = runId
            });
          return JsonConvert.DeserializeObject<Dictionary<string, object>>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
        }
    }
}
