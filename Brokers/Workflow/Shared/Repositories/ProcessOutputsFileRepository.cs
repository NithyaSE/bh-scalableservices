﻿using System;
using System.IO;
using System.Threading;
using SystemWrapper.IO;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using System.Reflection;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class ProcessOutputsFileRepository : IProcessOutputsRepository
    {
        #region Properties

        public string RunsFolder = "Runs";
        public string OutputFolder = "Output";

        static ReaderWriterLock rwl = new ReaderWriterLock();

        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public WorkflowSettings Settings { get; set; }

        #endregion

        #region Constructors

        public ProcessOutputsFileRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }


        #endregion

        public string GetItem(string runId)
        {
            var json = GetItemAsJson(runId);
            return json;
        }

        public void SaveItem(string runId, string dict)
        {
            if (!Directory.Exists(RunsFolder))
                Directory.CreateDirectory(RunsFolder);
            if (!Directory.Exists(Path.Combine(RunsFolder, OutputFolder)))
                Directory.CreateDirectory(Path.Combine(RunsFolder, OutputFolder));
            var runPath = Path.Combine(RunsFolder, OutputFolder, runId + ".json");
            WriteFileToRepository(runPath, dict, runId);
        }

        public string GetItemAsJson(string runId)
        {
            if (!Directory.Exists(RunsFolder) || !Directory.Exists(Path.Combine(RunsFolder, OutputFolder)))
                throw new Exception("The Process has no outputs");
            var runPath = Path.Combine(RunsFolder, OutputFolder, runId + ".json");
            if (!File.Exists(runPath))
                throw new Exception("The Process has no outputs");
            var json = ReadFileFromRepository(runPath);
            return json;
        }



        private void WriteFileToRepository(string path, string contents, string runId)
        {
            //LogUtility.WriteToLog($"Acquiring writer lock over repository  for runId { runId}");
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {

                FileIO.WriteAllText(path, contents);
            }
            finally
            {
                // Ensure that the lock is released.
                //LogUtility.WriteToLog($"Releasing writer lock over repository  for runId { runId}");
                rwl.ReleaseWriterLock();
            }

        }

        private void DeleteFileFromRepository(string path, string runId)
        {
            //LogUtility.WriteToLog($"Acquiring writer lock over repository for runId { runId}");
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {

                FileIO.Delete(path);
            }
            finally
            {
                // Ensure that the lock is released.
                //LogUtility.WriteToLog($"Releasing writer lock over repository for runId { runId}");
                rwl.ReleaseWriterLock();
            }

        }

        private string ReadFileFromRepository(string path)
        {
            // The thread will wait until the lock is acquired. We can add a timeout here.
            //LogUtility.WriteToLog($"Acquiring reader lock over repository");
            rwl.AcquireReaderLock(-1);
            try
            {
                // It is safe for this thread to read from the shared resource.
                return FileIO.ReadAllText(path);
            }
            finally
            {
                //LogUtility.WriteToLog($"Releasing reader lock over repository");
                // Ensure that the lock is released.
                rwl.ReleaseReaderLock();
            }
        }
    }

}
