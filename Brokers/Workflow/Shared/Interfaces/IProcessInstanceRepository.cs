﻿using System.Collections.Generic;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{

    public interface IProcessInstanceRepository
    {
        void Add(IWorkflowProcessInstance processInstance);

        void AddUpdate(string processInstanceName, IWorkflowProcessInstance processInstance);
        void Remove(string processInstanceName);

        IWorkflowProcessInstance GetItem(string processInstanceName);

        string GetItemAsJson(string processInstanceName);

        IWorkflowProcessInstance GetItemAtLocation(string location);

        List<IWorkflowProcessInstance> GetItems();
        
    }
}
