﻿using System.Collections.Generic;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{
    public interface ITaskSchemaRepository
    {
        void Add(dynamic item);

        void AddUpdate(string name, dynamic item);
        void Remove(string id);

        dynamic GetItem(string id);

        string GetItemAsJson(string id);

        List<dynamic> GetItems();

        List<dynamic> GetItems(string property);

    }

}
