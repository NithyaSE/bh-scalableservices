﻿using System.Collections.Generic;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{
    public interface IProcessRunsRepository
    {
        IWorkflowProcessInstance GetProcessInstanceFromRunId(string processInstanceName);
        void SaveProcessInstance(IWorkflowProcessInstance processInstance);
        IWorkflowTaskInstance GetTaskInstanceFromRunId(string runId, string taskInstanceName);
        List<IWorkflowTaskInstance > GetTaskIntancesFromRunId(string processInstanceName);
        string CreateTempTaskInstance(IWorkflowTaskInstance taskInstance, string runId);
        bool IsResumeTokenValid(string resumeToken, string runId);
        void UpdateWorkingSet(string taskName, string runId,string currentTaskTempFile, string body);
        void DehydrateTempTask(string runId, string currentTaskFileName, IWorkflowTaskInstance workflowTaskInstance);
        IWorkflowTaskInstance RehydrateTempTask(string runId, string currentTaskFileName);
        void RemoveTempTaskFile(string runId, string currentTaskFileName);
    }
}
