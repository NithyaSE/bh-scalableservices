﻿namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{

    public interface IProcessOutputsRepository
    {

        string GetItem(string runId);
        void SaveItem(string runId, string dict);

    }
}
