﻿using System.Collections.Generic;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{
    //Rename to IWorkflowProcessInstance
    public interface IWorkflowProcessInstance
    {
        [JsonProperty("name")]
        string Name { get;  }
        [JsonProperty("is_active")]
        bool IsActive { get; set; }
        [JsonProperty("task_execution_order")]
        List<string> TaskExecutionOrder { get; set; }
        [JsonProperty("on_error_execution_order")]
        List<string> OnErrorExecutionOrder { get; set; }
        // Update ProcessVariable to InputPV
        [JsonProperty("process_variables")]
        Dictionary<string, object> ProcessVariables { get; set; }
        string RunId { get; set; }
        object GetProcessVariable(string key);
        void SetProcessVariable(string key, object value);
        void SetProcessDisplayName(string name);
        Dictionary<string, object> SetOutputs();
        void ComputeProcessVariableKeywords(string timezone);
    }

}
