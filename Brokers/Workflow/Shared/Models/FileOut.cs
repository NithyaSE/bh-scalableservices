﻿using System.IO;
using System.Text;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{

    public class FileOut : IFileOut
    {
        public void WriteToFile(string fileName, string folderPath, StringBuilder content)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            File.WriteAllText(fileName, content.ToString());
        }
    }


    public interface IFileOut
    {
        void WriteToFile(string fileName, string folderPath, StringBuilder content);
    }
}
