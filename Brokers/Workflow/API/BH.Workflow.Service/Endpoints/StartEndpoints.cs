﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using BH.ScalableServices.Brokers.APIBase.Endpoints;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.Shared.Enums;
using BH.ScalableServices.Brokers.Workflow.ProcessBundler;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Shared.SerilogsLogging;
using Shared.ServerLibrary;
using Nancy;
using Newtonsoft.Json;
using Serilog;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class WFSStartEndpoints : StartEndpoints
    {
        private IProcessInstanceRepository ProcessInstanceRepo { get; set; }
        protected override string RouteVersion
        {
            get { return "v2.0"; }
        }
        /// <summary>
        /// 
        /// </summary>
        public WFSStartEndpoints(IRunDetailsRepository runRepo, IProcessInstanceRepository processRepo, WorkflowSettings settings, IProcessRunsRepository processRunsRepo) : base(runRepo)
        {
            ProcessInstanceRepo = processRepo;
            //Start end point for library folder (non subprocess processes)
            // set run log folder 
            Get[RouteVersion + "/start"] = parameters =>
            {
                try
                {
                    Log.Logger.AddMethodName().Debug("Summary='Process Bundles Location {@ProcessBundlesLibraryLocation}, Process Runs Location {@ProcessRunsLocation}'", settings.ProcessBundlesLibraryLocation, settings.ProcessRunsLocation);
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    var runId = Request.Query["run_id"].Value;
                    var errorText = "";
                    if (!IsRequestValid(processInstanceName, runId, out errorText))
                    {
                        return HandleErrorResponse(errorText, HttpStatusCode.BadRequest);
                    }
                    if (string.IsNullOrWhiteSpace(processInstanceName))
                    {
                        runId = System.Web.HttpUtility.UrlDecode(runId); // to handle '&'
                        // path used by dehydrated processes.  Want to be put on the queue, but not create a brand new instance
                        processInstanceName = GetProcessInstanceNameFromRunId(runId, runRepo, settings);
                        //validate the resume token if resume token provided on the request.
                        var resumeToken = Request.Query["resume_token"].Value;
                        if (!string.IsNullOrWhiteSpace(resumeToken))
                        {
                            if (processRunsRepo.IsResumeTokenValid(resumeToken, runId))
                            {
                                var processInstance = processRunsRepo.GetProcessInstanceFromRunId(runId);
                                processInstance.RunId = runId;
                                RemoveResumeToken(processInstance, processRunsRepo);
                            }
                        }
                    }
                    // runId is returned as null when the process is_active=false 
                    else
                    {
                        processInstanceName = System.Web.HttpUtility.UrlDecode(processInstanceName); // to handle '&'
                        runId = RunProcessBundler(processInstanceName, settings, ProcessBundleTypes.Library);
                    }
                    if (runId == null)
                    {
                        return ProcessNotActiveResponse(processInstanceName);
                    }
                    return Start(processInstanceName, runId, runRepo, settings);
                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }

            };
            Post[RouteVersion + "/start"] = parameters =>
            {
                try
                {
                    Log.Logger.AddMethodName().Debug("Summary='Process Bundles Location {@ProcessBundlesLibraryLocation}, Process Runs Location {@ProcessRunsLocation}'",settings.ProcessBundlesLibraryLocation,settings.ProcessRunsLocation);
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    var runId = Request.Query["run_id"].Value;
                    var errorText = "";
                    if (!IsRequestValid(processInstanceName, runId, out  errorText))
                    {
                        return HandleErrorResponse(errorText, HttpStatusCode.BadRequest);
                    }

                    if (string.IsNullOrWhiteSpace(processInstanceName))
                    {
                        runId = System.Web.HttpUtility.UrlDecode(runId); // to handle '&'
                        // path used by dehydrated processes.  Want to be put on the queue, but not create a brand new instance
                        processInstanceName = GetProcessInstanceNameFromRunId(runId, runRepo, settings);

                        // check if the request is for restarting a paused process
                        var resumeToken = Request.Query["resume_token"].Value;
                        if (!string.IsNullOrWhiteSpace(resumeToken))
                        {
                            ResumePauseTask(processRunsRepo, resumeToken, runId);
                        }

                    }
                    else
                    {
                        processInstanceName = System.Web.HttpUtility.UrlDecode(processInstanceName); // to handle '&'
                        runId = RunProcessBundler(processInstanceName, settings, ProcessBundleTypes.Library);
                    }

                    // runId is returned as null when the process is_active=false 
                    if (runId == null)
                    {
                        return ProcessNotActiveResponse(processInstanceName);
                    }
                    return Start(processInstanceName, runId, runRepo, settings);
                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }

            };
            //Start end point for subprocess folder (subprocess processes)
            // set run log folder 
            Get[RouteVersion + "/startsubprocess"] = parameters => ProcessStartSubProcess(settings, runRepo);
            Post[RouteVersion + "/startsubprocess"] = parameters => ProcessStartSubProcess(settings, runRepo);
        }

        private Response ProcessStartSubProcess(WorkflowSettings settings,IRunDetailsRepository runRepo)
        {
            try
            {

                var processInstanceName = Request.Query["process_instance_name"].Value;
                var partialProcessPath = Request.Query["partial_path"].Value;
                var runId = RunProcessBundler(processInstanceName, settings, ProcessBundleTypes.SubProcess, partialProcessPath);
                // runId is returned as null when the process is_active=false 
                if (runId == null)
                {
                    return ProcessNotActiveResponse(processInstanceName);
                }
                return Start(processInstanceName, runId, runRepo, settings);
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName().Error("Summary='Error Occured {@ex}'",ex);
                return HandleResponse(ex);
            }
        }


        private string GetProcessInstanceNameFromRunId(string runId, IRunDetailsRepository runRepo, WorkflowSettings settings)
        {
            runRepo.RunsFolder = Path.Combine(settings.ProcessRunsLocation);
            var runDetails = runRepo.GetItem(runId, false);
            if (runDetails == null)
                throw new Exception("run_id does not exist");
            return ((string)runId).Split('_')[0];

        }
        private void ResumePauseTask(IProcessRunsRepository processRunsRepo,string resumeToken,string runId)
        {
            if (!processRunsRepo.IsResumeTokenValid(resumeToken, runId)) return;
            var processInstance = processRunsRepo.GetProcessInstanceFromRunId(runId);
            processInstance.RunId = runId;
            var currentPathIndexString = processInstance.ProcessVariables.FirstOrDefault(x => x.Key == "$current_task_execution_order_index").Value.ToString();
            var currentPathErrorIndexString = processInstance.ProcessVariables.FirstOrDefault(x => x.Key == "$current_error_path_task_execution_order_index_index").Value.ToString();
            var taskName = "";
            var index = int.Parse(currentPathIndexString);
            var errorIndex = int.Parse(currentPathErrorIndexString);
            if (errorIndex != 0)
            {
                if (processInstance.OnErrorExecutionOrder != null)
                {
                    taskName = processInstance.OnErrorExecutionOrder.Skip(errorIndex).FirstOrDefault();
                }
            }
            else
            {
                taskName = processInstance.TaskExecutionOrder.Skip(index - 1).FirstOrDefault();
            }
            string body;
            using (var reader = new StreamReader(Request.Body))
            {
                body = reader.ReadToEnd();
            }

            var currentTaskTempFile = processInstance.GetProcessVariable("$current_task_file_name");
            processRunsRepo.UpdateWorkingSet(taskName, runId, currentTaskTempFile.ToString(), body);
            RemoveResumeToken(processInstance, processRunsRepo);
        }
        private void RemoveResumeToken(IWorkflowProcessInstance processInstance,IProcessRunsRepository processRunsRepo)
        {
            processInstance.ProcessVariables.Remove("$resume_token");
            processRunsRepo.SaveProcessInstance(processInstance);
        }
        /// <summary>
        /// Checks the HTTP request for valid parameters
        /// </summary>
        private bool IsRequestValid(string processInstanceName, string runId, out string errorText)
        {
            if (string.IsNullOrWhiteSpace(processInstanceName) && string.IsNullOrWhiteSpace(runId))
            {
                errorText = "Provide a process_instance_name.  Neither a process_instance_name or run_id found.";
                return false;
            }
            if (!string.IsNullOrWhiteSpace(processInstanceName) && !string.IsNullOrWhiteSpace(runId))
            {
                errorText = "Provide only a process_instance_name to start a new process. run_id is used internally.";
                return false;
            }
            errorText = "";
            return true;
        }private Response ProcessNotActiveResponse(string processInstanceName)
        {
            return HandleErrorResponse($"No run created for process instance {processInstanceName} as the process instance is currently set to inactive.", HttpStatusCode.BadRequest).WithContentType("text/plain");

        }
        private Response Start(string processInstanceName, string runId, IRunDetailsRepository runRepo, WorkflowSettings settings)
        {
            // runs folder is changed here to point to the run folder under ProcessRuns
            runRepo.RunsFolder = Path.Combine(settings.ProcessRunsLocation);
            Log.Logger.AddMethodName().Debug("Summary='RequestId:{@runID}, Time: {@time}, process_instance_name:{@processInstanceName}'",runId,DateTime.Now,processInstanceName);
            if (string.IsNullOrEmpty(processInstanceName))
                return HandleErrorResponse(
                    "Missing process_instance_name parameter.  Example: /start?process_instance_name=[process_instance_name].",
                    HttpStatusCode.BadRequest);
            var runDetails = StartProcess(runId, processInstanceName, processInstanceName, EnvironmentVariables.BHXWFSRequests,WorkerType.Workflow);
            if (runDetails == null)
                return HandleErrorResponse(
                    "Missing process_instance_name parameter.  Example: /start?process_instance_name=[process_instance_name].",
                    HttpStatusCode.BadRequest);
            var response = JsonConvert.SerializeObject(runDetails);
            Log.Logger.AddMethodName().Debug("Summary='RequestId:{@runID}, Time: {@time}, Response:{@response}'", runId, DateTime.Now, response);
            return Response.AsText(response).WithStatusCode(HttpStatusCode.OK).WithContentType("application/json");
        }
        /// <summary>
        /// Copies the process bundle to runs location. Only called if the a new process is put on queue.
        /// </summary>
        /// <param name="processInstanceName"></param>
        /// <param name="settings"></param>
        /// <param name="type"></param>
        /// <param name="processPartialLocation"></param>
        /// <returns></returns>
        private string RunProcessBundler(string processInstanceName, WorkflowSettings settings, ProcessBundleTypes type, string processPartialLocation = "")
        {  // search for process bundle
            var location = GetBundlesLocation(settings, processPartialLocation, type);
            var bundler = new ProcessBundle(processInstanceName, location, settings.ProcessRunsLocation, type);
            var bundlePath = bundler.LookupProcessBundle();
            if (bundlePath == null)
            {
                throw new Exception($"The process instance with name {processInstanceName} could not be found");
            }
            //get if the process is active
            if (!IsProcessActive(bundlePath))
            {
                return null;
            }
            var runId = $"{processInstanceName}_{Guid.NewGuid().ToString("N")}";
            bundler.CopyProcessBundleToProcessRun(bundlePath, runId);
            return runId;
        }
        private bool IsProcessActive(string location)
        {
            var processInstance = ProcessInstanceRepo.GetItemAtLocation(location);
            if (processInstance == null)
            {
                throw new Exception($"No process file found at {location}.");
            }
            return processInstance.IsActive;
        }
        private string GetBundlesLocation(WorkflowSettings settings, string processPartialLocation, ProcessBundleTypes type)
        {
            var location = "";
            switch (type)
            {
                case ProcessBundleTypes.Library: { location = settings.ProcessBundlesLibraryLocation; }; break;
                case ProcessBundleTypes.SubProcess: { location = Path.Combine(settings.ProcessBundlesSubProcLocation, processPartialLocation); }; break;
            }
            return location;
        }
    }
}