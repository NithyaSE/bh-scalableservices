﻿using System;
using System.Dynamic;
using BH.ScalableServices.Brokers.Workflow.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using Nancy;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessSchemaEndpoints : ResponseHandlerNancyModule
    {

        private string routeVersion = "v2.0";

        public IProcessSchemaRepository ProcessSchemaRepo { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public ProcessSchemaEndpoints(IProcessSchemaRepository processSchemaRepo)
        {
            ProcessSchemaRepo = processSchemaRepo;

            Get[routeVersion + "/processSchema"] = parameters =>
            {
                try
                {
                    var processSchema = (ExpandoObject) ProcessSchemaRepo.GetItem();

                    if (processSchema == null)
                    {
                        throw new WorkflowException("The Core Process Schema cannot be found - aka - processinstance_schema.json", HttpStatusCode.NotFound);
                    }

                    return Response.AsJson(processSchema).WithStatusCode(HttpStatusCode.OK);
                   
                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };
        }
        

    }

}
