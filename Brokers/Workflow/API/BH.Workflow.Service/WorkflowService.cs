﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared.SerilogsLogging;
using Serilog;
using Topshelf;

namespace BH.ScalableServices.Brokers.Workflow
{
    public class WorkflowService
    {
        public static void Main(string[] args)
        {
            
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .CreateLogger();
            try
            {
                ParseArgs(args.ToList());
                HostFactory.Run(x =>
                {
                    x.Service<WorkflowServiceRunner>(s =>
                    {
                        s.ConstructUsing(name => new WorkflowServiceRunner());
                        s.WhenStarted(tc => tc.Start());
                        s.WhenStopped(tc => tc.Stop());
                    });
                    x.RunAsLocalSystem();

                    x.SetDescription("Bloodhound Workflow Service - Automating Awesomeness Daily!");
                    x.SetDisplayName("Workflow Service Broker");
                    x.SetServiceName("WorkflowServiceBroker");
                });
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName().Fatal("Summary='Fatal Workflow Service Error' Details='{@ex}'" , ex);
            }
        }
        public static void ParseArgs(List<string> args)
        {
            if (args.Count <= 0) return;
            DisplayHelp(args);
        }
        public static void DisplayHelp(List<string> args)
        {
            var helpIndex = args.FindIndex(x => x.ToUpper() == "-HELP");
            if (helpIndex >= 0)
            {
                Console.WriteLine("Available Options:\n" +
                                  " None at the moment.");
                Environment.Exit(0);
            }
        }
    }


}
