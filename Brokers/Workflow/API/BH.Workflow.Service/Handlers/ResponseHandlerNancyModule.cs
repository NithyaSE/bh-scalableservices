﻿using System;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using Nancy;

namespace BH.ScalableServices.Brokers.Workflow.Handlers
{
    public class ResponseHandlerNancyModule : NancyModule
    {
        public Response HandleResponse(Exception ex)
        {
            var response = new WorkflowResponse();

            var exception = ex as WorkflowException;
            if (exception != null)
            {
                response = exception.Response;
            }
            else
            {
                response.Error = ex.ToString();
                response.StatusCode = HttpStatusCode.InternalServerError;
            }



            return Response.AsJson(response).WithStatusCode(response.StatusCode);
        }

        public Response HandleResponse(string message, HttpStatusCode statusCode)
        {
            var response = new WorkflowResponse()
            {
                StatusCode = statusCode,
                Error = message
            };

            return Response.AsJson(response).WithStatusCode(statusCode);
        }

        public Response HandleErrorResponse(string message, HttpStatusCode statusCode)
        {
            var response = new WorkflowResponse()
            {
                StatusCode = statusCode,
                Error = message
            };

            return Response.AsJson(response).WithStatusCode(statusCode);
        }

    }



}
