﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BH.ScalableServices.Brokers.Workflow.Helpers
{
    //Using this class for name length validation and not adding the validations on the models themselves as the validations are
    //needed to ensure no long path exceptions(as we are using file system) are thrown and the limits should be removed once a move to a different data store is made.
    public static class ValidationHelpers
    {
        private static int _maxProcessNameLength;
        private static int _maxTaskNameLength;
        private static int _maxSchemaNameLength;

        static ValidationHelpers()
        {
            _maxProcessNameLength = int.Parse(ConfigurationManager.AppSettings["MaxProcessNameLength"]);
            _maxTaskNameLength = int.Parse(ConfigurationManager.AppSettings["MaxTaskNameLength"]);
            _maxSchemaNameLength = int.Parse(ConfigurationManager.AppSettings["MaxSchemaNameLength"]);
        }

        public static void ValidateProcessInstanceName(string name)
        {
            if (name.Length > _maxProcessNameLength)
            {
                throw new Exception($"Process instance name cannot be longer than {_maxProcessNameLength} characters.");

            }
        }
        public static void ValidateTaskInstanceName(string name)
        {
            if (name.Length > _maxTaskNameLength)
                throw new Exception($"Task instance name cannot be longer than {_maxTaskNameLength} characters.");
        }
        public static void ValidateSchemaName(string name)
        {
            if (name.Length > _maxSchemaNameLength)
                throw new Exception($"Schema name cannot be longer than {_maxSchemaNameLength} characters.");
        }
    }
}
