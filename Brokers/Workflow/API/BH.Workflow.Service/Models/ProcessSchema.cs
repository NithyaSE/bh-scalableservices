﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class ProcessSchema
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
    }
}
