﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class ProcessDefinition
    {
        public ProcessDefinition()
        {
            TaskDefinitions = new List<TaskDefinition>();
        }

        public JObject ProcessInstanceSchema { get; set; }
        public JObject ProcessInstance { get; set; }
        public List<TaskDefinition> TaskDefinitions { get; set; }
    }

}
