﻿using System.Diagnostics;
using BH.ScalableServices.Brokers.Workflow.Interfaces;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class ProcessRunner : IProcessRunner
    {
        public void RunProcess(string runId, string args)
        {

            var arguments = runId;
            arguments += args != "" ? " " + args : "";

            var processStartInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                FileName = "BH.Workflow.Process.exe",
                CreateNoWindow = true,
                Arguments = arguments
            };

            var process = new Process { StartInfo = processStartInfo };
            process.Start();
        }
    }
}
