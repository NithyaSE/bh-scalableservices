﻿using System.Collections.Generic;
using Nancy.Security;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class BhUser : IUserIdentity
    {
        public BhUser(string user)
        {
            UserName = user;
        }


        public string UserName { get; private set; }
        public IEnumerable<string> Claims { get; private set; }
    }
}
