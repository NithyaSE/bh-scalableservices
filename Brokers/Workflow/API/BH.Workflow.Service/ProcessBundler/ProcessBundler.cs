﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BH.ScalableServices.Brokers.Workflow.ProcessBundler
{
    public class ProcessBundle
    {
        public string Name { get; set; }
        public ProcessBundleTypes Type { set; get; }
        public string ProcessBundlesLocation  { get; set; }
        public string ProcessRunsLocation { get; set; }
        public ProcessBundle(string name,string processBundlesLocation,string processRunsLocation,ProcessBundleTypes type)
        {
            Name = name;
            Type = type;
            ProcessBundlesLocation = processBundlesLocation;
            ProcessRunsLocation = processRunsLocation;
        }

        public string LookupProcessBundle()
        {
            IEnumerable<string> subDirs = null;
            if (!string.IsNullOrEmpty(this.Name))
            {
                subDirs = Directory.GetDirectories(this.ProcessBundlesLocation, this.Name, SearchOption.AllDirectories);
            }
            if (subDirs != null && subDirs.Any())
                return subDirs.FirstOrDefault();
            else return null;
        }

        public void CopyProcessBundleToProcessRun(string processBundlePath,string runId)
        {
            // Get the subdirectories for the specified directory.
            var dir = new DirectoryInfo(processBundlePath);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException($"Directory at location {processBundlePath} does not exist or could not be found.");
            }

            var dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            var destinationPath = Path.Combine(this.ProcessRunsLocation, runId);

            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }

            // Get the files in the directory and copy them to the new location.
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var temppath = Path.Combine(destinationPath, file.Name);
                file.CopyTo(temppath, true);
            }

            // copy subdirectories and their contents to new location.
            foreach (var subdir in dirs)
                {
                    var temppath = Path.Combine(destinationPath, subdir.Name);
                    CopyProcessBundleToProcessRun(subdir.FullName, temppath);
                }
           
        }
    }
}
