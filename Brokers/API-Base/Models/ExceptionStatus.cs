﻿using System;
using Nancy;

namespace BH.ScalableServices.Brokers.APIBase.Models
{
    public class ExceptionStatus : Exception
    {
        public ExceptionResponse Response { get; set; }

        public ExceptionStatus(ExceptionResponse response)
            : base(response.Error)
        {
            Response = response;
        }

        public ExceptionStatus(string message, HttpStatusCode statusCode)
            : base(message)
        {
            Response = new ExceptionResponse
            {
                Error = message,
                StatusCode = statusCode
            };
        }
    }
}
