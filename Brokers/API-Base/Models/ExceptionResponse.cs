﻿using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.APIBase.Models
{
    public class ExceptionResponse
    {
        public ExceptionResponse()
        {
            Error = null;
        }
        
        [JsonProperty("status_code")]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
