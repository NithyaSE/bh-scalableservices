﻿namespace BH.ScalableServices.Brokers.APIBase.Helpers
{
    public class PropertyCopier<TSource, TDest> where TSource : class where TDest : class
    {
        public static void Copy(TSource source, TDest destination)
        {
            var sourceProperties = source.GetType().GetProperties();
            var destProperties = destination.GetType().GetProperties();
            foreach (var sourceProperty in sourceProperties)
            {
                foreach (var destProperty in destProperties)
                {
                    if (sourceProperty.Name == destProperty.Name && sourceProperty.PropertyType == destProperty.PropertyType)
                    {
                        destProperty.SetValue(destination, sourceProperty.GetValue(source));
                        break;
                    }
                }
            }
        }
    }

}
