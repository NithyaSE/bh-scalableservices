﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BH.ScalableServices.Brokers.APIBase.Handlers;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.APIBase.Models;
using BH.ScalableServices.Brokers.Shared.Models;
using Shared.SerilogsLogging;
using Nancy;
using Newtonsoft.Json;
using Serilog;

namespace BH.ScalableServices.Brokers.APIBase.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class DetailsEndpoints : ResponseHandlerNancyModule
    {
        protected abstract string RouteVersion { get; }

        private IRunDetailsRepository RunRepo { get; set; }
        
        public DetailsEndpoints(IRunDetailsRepository runRepository)
        {
            RunRepo = runRepository;

            Get[RouteVersion + "/details"] = parameters =>
            {
                try
                {
                    var includeLogEntriesQueryParam = Request.Query["include_log_entries"].Value;
                    var includeLogEntries = true;
                    if (includeLogEntriesQueryParam != null)
                    {
                        if (!bool.TryParse(includeLogEntriesQueryParam, out includeLogEntries))
                        {
                            return HandleErrorResponse("Invalid value of include_log_entries parameter. Only bool values accepted.", HttpStatusCode.BadRequest);
                        }
                    }
                    var processInstanceName = Request.Query["name"].Value;
                    if (processInstanceName != null)
                    {
                        var runs = (List<ScalableServiceRunDetails>) GetRunDetails(processInstanceName,includeLogEntries);

                        return Response.AsText(JsonConvert.SerializeObject(runs)).WithStatusCode(HttpStatusCode.OK).WithContentType("application/json");
                    }

                    processInstanceName = Request.Query["process_instance_name"].Value;
                    if (processInstanceName != null)
                    {
                        var runs = (List<ScalableServiceRunDetails>)GetRunDetails(processInstanceName, includeLogEntries);

                        return Response.AsText(JsonConvert.SerializeObject(runs)).WithStatusCode(HttpStatusCode.OK).WithContentType("application/json");
                    }

                    var runId = Request.Query["run_id"].Value;
                    if (runId != null)
                    {
                        var run = (ScalableServiceRunDetails) GetSpecificRunDetails(runId, includeLogEntries);

                        //jsonProperty name replaced works with newtonsoft, but not the nancy serialization (IsExecuting instead of is_executing)
                        return Response.AsText(JsonConvert.SerializeObject(run)).WithStatusCode(HttpStatusCode.OK).WithContentType("application/json");

                        //return Response.AsJson(run).WithStatusCode(HttpStatusCode.OK);
                    }

                    return HandleErrorResponse("Must provide either the process_instance_name or run_id parameters in order to get run details.", HttpStatusCode.BadRequest);

                }
                catch (Exception ex)
                {
                    Log.Logger.AddMethodName().Error("Time:{@time} Response:{@ex}", DateTime.UtcNow,ex );
                    return HandleResponse(ex);
                }


            };
            Delete[RouteVersion + "/details"] = parameters =>
            {
                try
                {
                    var runId = Request.Query["run_id"].Value;
                    if (runId == null)
                    {
                        throw new ExceptionStatus("run_id must be specified: ", HttpStatusCode.BadRequest);
                    }
                    RunRepo.Remove(runId);
                    return HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };
            Post[RouteVersion + "/logentries"] = parameters =>
            {
                try
                {
                    var runId = Request.Query["run_id"].Value;

                    if (runId == null)
                    {
                        throw new ExceptionStatus("run_id must be specified: " , HttpStatusCode.BadRequest);
                    }

                    var userAddress = this.Request.UserHostAddress;

                    string body;
                    using (var reader = new StreamReader(Request.Body))
                    {
                        body = reader.ReadToEnd();
                    }

                    if (body.StartsWith("{"))
                    {
                        var log = JsonConvert.DeserializeObject<ScalableServiceLogEntry>(body);

                        RunRepo.AddLogEnrty(runId, log.Message, string.Format("{0} {1}", log.Source, userAddress), log.Timestamp);
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        var logs = JsonConvert.DeserializeObject<List<ScalableServiceLogEntry>>(body);

                        foreach (var log in logs)
                        {
                            RunRepo.AddLogEnrty(runId, log.Message, string.Format("{0} {1}", log.Source, userAddress), log.Timestamp);
                        }
                        return HttpStatusCode.OK;
                    }

                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };
            Patch[RouteVersion + "/Status"] = parameters =>
            {
                try
                {
                    var runId = Request.Query["run_id"].Value;
                    if (runId == null)
                    {
                        throw new ExceptionStatus("run_id must be specified: ", HttpStatusCode.BadRequest);
                    }
                    string body;
                    using (var reader = new StreamReader(Request.Body))
                    {
                        body = reader.ReadToEnd();
                    }

                    Log.Information(string.Format("Start PATCH {0} Body: {1} RunId: {2}", Request.Path, body, runId));

                    var status = JsonConvert.DeserializeObject<ScalableServiceStatus>(body);

                    RunRepo.UpdateStatus(runId, status);

                    Log.Information(string.Format("End PATCH {0} Body: {1} RunId: {2}", Request.Path, body, runId));

                    return HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error Patching Status");
                    return HandleResponse(ex);
                }
            };
        }
        private List<ScalableServiceRunDetails> GetRunDetails(string processInstanceName, bool includeLogEntries)
        {
            var runs = (List<ScalableServiceRunDetails>)RunRepo.GetItems(processInstanceName,includeLogEntries);

            if (!runs.Any() || runs == null)
                throw new ExceptionStatus("There are no run details under the given process_name: " + processInstanceName, HttpStatusCode.NotFound);
            return runs;
        }
        private ScalableServiceRunDetails GetSpecificRunDetails(string runId, bool includeLogEntries)
        {
            var run = RunRepo.GetItem(runId,includeLogEntries);

            if (run == null)
                throw new ExceptionStatus("There are no run details for the given run_id: " + runId, HttpStatusCode.NotFound);

            return run;
        }
    }
}
