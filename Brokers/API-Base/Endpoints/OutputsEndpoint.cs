﻿using System;
using System.IO;
using BH.ScalableServices.Brokers.APIBase.Handlers;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using Nancy;

namespace BH.ScalableServices.Brokers.APIBase.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class OutputsEndpoint : ResponseHandlerNancyModule
    {
        protected abstract string RouteVersion { get; }

        private IOutputsRepository OutputRepo { get; set; }
        
        public OutputsEndpoint(IOutputsRepository outputRepo)
        {
            OutputRepo = outputRepo;

            Get[RouteVersion + "/outputs"] = parameters =>
            {
                try
                {
                    var runId = Request.Query["run_id"].Value;
                    if (runId != null)
                    {
                        var run = (string)OutputRepo.GetItem(runId);

                        //jsonProperty name replaced works with newtonsoft, but not the nancy serialization (IsExecuting instead of is_executing)
                        return Response.AsText(run).WithStatusCode(HttpStatusCode.OK)
                            .WithContentType("application/json");

                        //return Response.AsJson(run).WithStatusCode(HttpStatusCode.OK);
                    }

                    return base.HandleErrorResponse("Must provide run_id parameters in order to get outputs.",
                        HttpStatusCode.BadRequest);

                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }


            };

            Post[RouteVersion + "/outputs"] = parameters =>
            {
                try
                {
                    var runId = Request.Query["run_id"].Value;

                    if (string.IsNullOrWhiteSpace(runId))
                        throw new Exception("Must provide a run_id to post outputs.");

                    string body;
                    using (var reader = new StreamReader(Request.Body))
                    {
                        body = reader.ReadToEnd();
                    }

                    OutputRepo.SaveItem(runId, body);
                    return HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };
        }
    }
}
