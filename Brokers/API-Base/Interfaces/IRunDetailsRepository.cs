﻿using System;
using System.Collections.Generic;
using BH.ScalableServices.Brokers.Shared.Models;

namespace BH.ScalableServices.Brokers.APIBase.Interfaces
{
    public interface IRunDetailsRepository
    {
        string RunsFolder { get; set; }
        void AddRunDetails(ScalableServiceRunDetails runDetails);
        void AddStatus(string runId,ScalableServiceStatus status);
        
        void AddUpdateStatus(string runId, ScalableServiceStatus status, string processInstanceName);
        void Remove(string runId);

        ScalableServiceRunDetails GetItem(string runId,bool includeLogEntries);

        string GetItemAsJson(string id,bool includeLogEntries);

        List<ScalableServiceRunDetails> GetItems(bool includeLogEntries);

        List<ScalableServiceRunDetails> GetItems(string property,bool includeLogEntries);

        void AddLogEnrty(string runId, string message, string source, DateTimeOffset time);

        void UpdateStatus(string runId, ScalableServiceStatus status);
    }
}
