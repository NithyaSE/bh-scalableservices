﻿using System;
using System.Collections.Generic;
using BH.ScalableServices.Brokers.Shared.Models;

namespace BH.ScalableServices.Brokers.APIBase.Interfaces
{
    public interface IOutputsRepository
    {
        string GetItem(string runId);
        void SaveItem(string runId, string dict);
    }
}
