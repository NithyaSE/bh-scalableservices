﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using SystemWrapper.IO;
using BH.ScalableServices.Brokers.APIBase.Interfaces;

namespace BH.ScalableServices.Brokers.APIBase.Repositories
{
    /// <summary>
    /// Repository used for reading and writing outputs
    /// </summary>
    public class OutputsRepository : IOutputsRepository
    {
        public string RunsFolder = "Runs";
        public string OutputFolder = "Output";
        static ReaderWriterLock rwl = new ReaderWriterLock();
        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }
        public IPathWrap PathWrap { get; set; }
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="fileWrap"></param>
        /// <param name="dirWrap"></param>
        /// <param name="path"></param>
        public OutputsRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap,IPathWrap path)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            PathWrap = path;
        }
        
        public string GetItem(string runId)
        {
            var json = GetItemAsJson(runId);
            return json;
        }

        public void SaveItem(string runId, string dict)
        {
            if (!DirIO.Exists(RunsFolder))
                DirIO.CreateDirectory(RunsFolder);
            if (!DirIO.Exists(PathWrap.Combine(RunsFolder, OutputFolder)))
                DirIO.CreateDirectory(PathWrap.Combine(RunsFolder, OutputFolder));
            var runPath = Path.Combine(RunsFolder, OutputFolder, runId + ".json");
            WriteFileToRepository(runPath, dict, runId);
        }

        public string GetItemAsJson(string runId)
        {
            if (!DirIO.Exists(RunsFolder) || !Directory.Exists(PathWrap.Combine(RunsFolder, OutputFolder)))
                throw new Exception("The Process has no outputs");
            var runPath = Path.Combine(RunsFolder, OutputFolder, runId + ".json");
            if (!FileIO.Exists(runPath))
                throw new Exception("The Process has no outputs");
            var json = ReadFileFromRepository(runPath);
            return json;
        }
        private void WriteFileToRepository(string path, string contents, string runId)
        {
            //LogUtility.WriteToLog($"Acquiring writer lock over repository  for runId { runId}");
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {

                FileIO.WriteAllText(path, contents);
            }
            finally
            {
                // Ensure that the lock is released.
                //LogUtility.WriteToLog($"Releasing writer lock over repository  for runId { runId}");
                rwl.ReleaseWriterLock();
            }

        }
        private string ReadFileFromRepository(string path)
        {
            rwl.AcquireReaderLock(-1);
            try
            {
                // It is safe for this thread to read from the shared resource.
                return File.ReadAllText(path);
            }
            finally
            {
                rwl.ReleaseReaderLock();
            }
        }

    }
}

  